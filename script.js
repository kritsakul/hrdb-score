// JavaScript Document
var xmlHttp;
function createXMLHttpRequest() {
    if (window.ActiveXObject) {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    } else if (window.XMLHttpRequest) {
        xmlHttp = new XMLHttpRequest();
    }
}


// ---- เปลี่ยนสถานะ ผู้ลงทะเบียน
// ไปยัง meeting-manage-name-status.php (add meeting save)
function funcChangeNameStatus(tmpStatus, tmpRegID) {       

    var tmpComment = "";
    if (tmpStatus == '0') {
        var tmpComment = prompt("กรุณาระบุสาเหตุ", "ห้องเต็ม");
        if (tmpComment == "") {
            alert("กรุณาระบุสาเหตุ");
            return;
        } else if (tmpComment === null) {
            return;
        }
    }

    var tmpDivID = 'div' + tmpRegID;
    document.getElementById(tmpDivID).innerHTML = "<img src='img/loader.gif'>";

    var tmpData = tmpStatus + "|||" + tmpRegID + "|||" + tmpComment;
    var totalData = encodeURIComponent(tmpData);
    createXMLHttpRequest();
    xmlHttp.onreadystatechange = stateFuncChangeNameStatus;
    xmlHttp.open("POST", "manage-name-status.php?dummy=" + Math.random(), true);
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.send("totalData=" + totalData);
}

function stateFuncChangeNameStatus() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {

        var tmpMainData = xmlHttp.responseText;
        var tmpDetail = tmpMainData.split("|||");
        var tmpDivID = 'div' + tmpDetail[2];

       if (jQuery.trim(tmpDetail[0]) === "1") {
            var tmpStatus = tmpDetail[1];
            if (tmpStatus == 1) { //หมายความว่า อนุมัติแล้ว
                document.getElementById(tmpDivID).innerHTML = "<button onclick=\"funcChangeNameStatus('1','" + tmpDetail[2] + "')\" class='btn btn-fill  btn-warning btn-xs'>รออนุมัติ</button>&nbsp;";
                document.getElementById(tmpDivID).innerHTML += "<button onclick=\"funcChangeNameStatus('2','" + tmpDetail[2] + "')\" class='btn btn-fill btn-xs'>อนุมัติ</button>&nbsp";
                document.getElementById(tmpDivID).innerHTML += "<button onclick=\"funcChangeNameStatus('0','" + tmpDetail[2] + "')\" class='btn btn-fill btn-xs'>ไม่อนุมัติ</button>";
            } else if (tmpStatus == 2) {//หมายความว่า อนุมัติแล้ว
                document.getElementById(tmpDivID).innerHTML = "<button onclick=\"funcChangeNameStatus('1','" + tmpDetail[2] + "')\" class='btn btn-fill btn-xs'>รออนุมัติ</button>&nbsp;";
                document.getElementById(tmpDivID).innerHTML += "<button onclick=\"funcChangeNameStatus('2','" + tmpDetail[2] + "')\" class='btn  btn-fill btn-success btn-xs'>อนุมัติ</button>&nbsp";
                document.getElementById(tmpDivID).innerHTML += "<button onclick=\"funcChangeNameStatus('0','" + tmpDetail[2] + "')\" class='btn btn-fill btn-xs'>ไม่อนุมัติ</button>";
            } else if (tmpStatus == 0) {//หมายความว่า ยกเลิก
                document.getElementById(tmpDivID).innerHTML = "<button onclick=\"funcChangeNameStatus('1','" + tmpDetail[2] + "')\" class='btn btn-fill btn-xs'>รออนุมัติ</button>&nbsp;";
                document.getElementById(tmpDivID).innerHTML += "<button onclick=\"funcChangeNameStatus('2','" + tmpDetail[2] + "')\" class='btn btn-fill btn-xs'>อนุมัติ</button>&nbsp";
                document.getElementById(tmpDivID).innerHTML += "<button onclick=\"funcChangeNameStatus('0','" + tmpDetail[2] + "')\" class='btn  btn-fill btn-danger btn-xs'>ไม่อนุมัติ</button>";
                document.getElementById(tmpDivID).innerHTML += "<div class=\"text-danger font12\">(" + tmpDetail[3] + ")</div>";
            }
        } else {
            alert("ขออภัยค่ะ!!! อาจมีบางอย่างผิดพลาด ลองทำการบันทึกใหม่");

        }

    }
}



// ---- meeting-file-save.php (แนบไฟล์)
function funcMeetingFile() {    
    var tmpFilename = $('#txtFilename').val();
    var tmpActID = $('#txtActID').val();  
    if (tmpFilename === "" || tmpActID === ""){
        swal("ไม่มีไฟล์ !!!", 'กรุณาเลือกไฟล์ที่ต้องการอัพโหลด ด้วยครับผม', "warning")       
        return;
    }    
    var tmpData = tmpFilename + "|||" + tmpActID;
    var totalData = encodeURIComponent(tmpData);
    createXMLHttpRequest();
    xmlHttp.onreadystatechange = stateFuncMeetingFile;
    xmlHttp.open("POST", "manage-file-save.php?dummy=" + Math.random(), true);
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.send("totalData=" + totalData);
}

function stateFuncMeetingFile() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
        var tmpMainData = xmlHttp.responseText;
        if (jQuery.trim(tmpMainData) === '1') {           
            swal("อัพโหลดไฟล์ เสร็จสมบูรณ์ !!!", '', "success")
            .then((value) =>{
                 location.reload();
            })                    
        }
    }
}

// ---- meeting-file-del.php (ลบไฟล์)
function funcMeetingFileDelete(fileno, filename) {
    
    //alert(tmpData);    
//    var r = confirm("กรณายืนยันว่า คุณต้องการลบไฟล์ นี้ใช่หรือไม่");
//    if (r == false) {
//        return;
//    }
    var tmpData = fileno + "|||" + filename;
    var totalData = encodeURIComponent(tmpData);
    createXMLHttpRequest();
    xmlHttp.onreadystatechange = stateFuncMeetingFileDelete;
    xmlHttp.open("POST", "manage-file-del.php?dummy=" + Math.random(), true);
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xmlHttp.send("totalData=" + totalData);
}

function stateFuncMeetingFileDelete() {
    if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
        var tmpMainData = xmlHttp.responseText;
        if (jQuery.trim(tmpMainData) === '1') {
            location.reload();
        }
    }
}


