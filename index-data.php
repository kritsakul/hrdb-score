<?php
    
require_once 'connections/conn.php';
require_once 'function.inc.php';      

$sql = "
    SELECT 
        *
    FROM 
        meet_activity
    WHERE
        meet_activity.act_status <> '0' AND
        meet_activity.act_status <> '4'		
    ORDER BY 
        FIELD(act_status, 1, 2, 4, 3),
        act_stop_register ASC
";
$query = $conn->prepare($sql);
$query->execute();

//ถ้าระบบยังไม่มีข้อมูล
$i = 0;
    
$tmpJson = "";
$tmpJson = "[";

while ($result = $query->fetch(PDO::FETCH_ASSOC)) {

        $i++;
        $tmpActID = $result['act_id'];
        $tmpStatus = $result['act_status'];
        $tmpDepartment = $result['act_department'];

        //เช็คจำนวนรุ่น
        $sqlGen = "SELECT gen_no FROM meet_activity_gen WHERE act_id='$tmpActID' AND gen_status<>'0'   " ;
        $rsGen = $conn->prepare($sqlGen);
        $rsGen->execute();
        $tmpTotalGen = $rsGen->rowCount();                                                                       

        if ($tmpStatus == 5) {
            $tmpStatusShow = "<span  style='color:red'>ยกเลิกการจัดกิจกรรม</span>";
        } else if ($tmpStatus == 1) {
            $tmpStatusShow = "<span style='color:green'>เปิดลงทะเบียน</span>";
        } else if ($tmpStatus == 2) {
            $tmpStatusShow = "<span style='color:red'>ปิดการลงทะเบียน</span>";
        } else if ($tmpStatus == 3) {
            $tmpStatusShow = "<span style='color:blue'>กิจกรรมเสร็จสมบูรณ์</span>";
        } else if ($tmpStatus == 4) {
            $tmpStatusShow = "<span style='color:#ff6600'>ยังไม่เปิดให้ลงทะเบียน</span>";
        }

        if ($tmpStatus == '0') {
            $tmpDateRegShow = "(" . $result['act_comment'] . ")";
        }

        //เช็คจำนวนรายชื่อผู้ลงทะเบียน - ทั้งหมด
        $sql = "
            SELECT 
                reg_status,
                count(reg_status) as count_status
            FROM 
                meet_register, 
                meet_activity_gen 
            WHERE 
                meet_register.gen_no = meet_activity_gen.gen_no AND
                meet_register.act_id = :tmpactid AND
                meet_activity_gen.gen_status = '1'
            GROUP BY
                reg_status
        ";
        $stmt = $conn->prepare($sql);
        $stmt->bindParam(':tmpactid', $tmpActID);
        $stmt->execute();                                 
        $tmpCountApprove = 0;
        $tmpCount = 0;
        while ($rsCount = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($rsCount['reg_status'] == 2) {
                $tmpCountApprove = $rsCount['count_status'];
            }
            $tmpCount += $rsCount['count_status'];            
        }            
        
        $tmpTitle = $result['act_title'];
        $tmpMainTitle =  "<a  style='text-decoration:underline; font-size:16px;' href='detail.php?actid=" . $tmpActID . " '>" . $tmpTitle . "</a>";                                       
        $sql = "SELECT 
                *
            FROM 
                meet_activity_gen
            WHERE 
                act_id='$tmpActID' 
                AND gen_status<>'0' 
            ORDER BY gen_index";
        $rsgen = $conn->prepare($sql);
        $rsgen->execute();
        if($query->rowCount() > 0){
            $tmpGen = "";
            while ($resultgen = $rsgen->fetch(PDO::FETCH_ASSOC)) {                        
                $tmpGenNo = $resultgen['gen_no'];
                $tmpIndex = $resultgen['gen_index'];
                $tmpPlace = $resultgen['gen_place'];
                $tmpStartDate = $resultgen['gen_start_date'];
                $tmpArrStartDate = explode("-",$tmpStartDate);
                $tmpStartDateShow = $tmpArrStartDate[2] . " " . $thaiShortMonthArr[$tmpArrStartDate[1]] . " " . ($tmpArrStartDate[0] + 543);

                $tmpStopDate = $resultgen['gen_stop_date'];
                $tmpArrStopDate = explode("-",$tmpStopDate);
                $tmpStopDateShow = $tmpArrStopDate[2] . " " . $thaiShortMonthArr[$tmpArrStopDate[1]] . " " . ($tmpArrStopDate[0] + 543);

                $tmpStartTime = substr($resultgen['gen_start_time'],0,5);
                $tmpStopTime = substr($resultgen['gen_stop_time'],0,5);
                $tmpTargetPerson = $resultgen['gen_target_person'];

                if($rsgen->rowCount() <= 1){ //แปลว่ามีเพียงรุ่นเดียว เราจะไม่แสดงคำว่า รุ่นที่                        
                    $tmpGen = $tmpStartDateShow . " - " .  $tmpStopDateShow . " ณ " . $tmpPlace . "<br>";
                }else{
                    $tmpGen .= "<b>รุ่นที่ " . $tmpIndex . "</b> - " . $tmpStartDateShow . " - " .  $tmpStopDateShow . " ณ " . $tmpPlace . "<br>";
                }

            }//END WHILE
        }//END IF                                                    

        $tmpJson .= "{";
        $tmpJson .= "\"no\" : \"<b>$i</b>\",";   
        $tmpJson .= "\"title\" : \"$tmpMainTitle<br>$tmpGen\",";   
        $tmpJson .= "\"gen\" : \"$tmpTotalGen\",";   
        $tmpJson .= "\"register\" : \"$tmpCount\",";   
        $tmpJson .= "\"approve\" : \"$tmpCountApprove\",";
        $tmpJson .= "\"status\" : \"$tmpStatusShow\","; 
        $tmpJson .= "\"code\" : \"$tmpActID\"";
        $tmpJson .= "},";   
                                                                            
}
        
   
$tmpJson = substr($tmpJson,0,-1); 
$tmpJson .= "]";   

echo $tmpJson;      
    
?>