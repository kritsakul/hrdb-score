<!doctype html>
<html lang="en">
    <head>
        
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />        
        <title>สร้างประกาศผลคะแนน - ระบบดูผลคะแนนสอบออนไลน์ กองบริหารทรัพยากรบุคคล สป.</title>

        <?php
            require_once 'connections/conn.php';
            require_once 'redirect.inc.php';
            require_once 'header.inc.php';
        ?>
        
        <style>
            .header{
                padding:20px !important;
            }
           th{
                font-size:16px;
            }
            .font16 .control-label{
                font-size:16px;
                color:#000;
            }
            ::placeholder {
                color:red;
            }
            .error{
                color:#FF0000 !important;
            }
            .text-red{
                color:#F00 !important;
            }            
        </style>
        
    </head>
    <body>

        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">
                        
                        <div class="row">                            
                            <div class="col-md-12">
                                
                                <div class="card">
                                               
                                    
                                    <p style="font-size:24px; padding:20px 10px 0px 20px;" class="title">
                                        <i class="pe-7s-note2"></i> <u><a href="manage.php" style="color:green;"> จัดการข้อมูลประกาศผลคะแนน</a></u> > สร้างประกาศผลคะแนน
                                    </p>
                                    
                                    <hr>
                                    
                                    <div style="margin-top:30px; padding-bottom:50px;">
                                         
                                                <!-- form start -->
                                                <form id="mainForm" class="form-horizontal">
                                                    
                                                    <div class="box-body font16">
                                                        <div class="container-fluid">
                                                            <input type="hidden" id="txtUsername" name="txtUsername" value="<?php echo $tmpMainUsername ?>">
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 control-label  text-red">*ตำแหน่งที่สอบ</label>
                                                                <div class="col-sm-8">
                                                                    <textarea type="text" maxlength="900"  class="form-control font16 required" required  id="txtTitle" name="txtTitle" placeholder="ใส่ชื่อตำแหน่งที่สอบ เช่น ตำแหน่งนักจัดการงานทั่วไปปฏิบัติการ นักวิชาการเงินและบัญชีปฏิบัติการ นักวิชาการพัสดุปฏิบัติการ " autocomplete="off" value="" ></textarea>
                                                                </div> 
                                                            </div>                

                                                            <div class="form-group">
                                                                <label for="inputPassword3" class="col-sm-3 control-label  text-red">*วันที่สอบ</label>
                                                                <div class="col-sm-8">                                    
                                                                    <input type="text" class="form-control font16  pull-right selector" id="txtExamDate" name="txtExamDate" required autocomplete="off"  placeholder="วันที่ทำการสอบ" value=""> 
                                                                </div>                           
                                                            </div>    
                                                            
                                                            <div class="form-group">
                                                                <label for="inputEmail3" class="col-sm-3 control-label  text-red">*คะแนนเต็ม</label>
                                                                <div class="col-sm-8">
                                                                    <input type="text" maxlength="5"  class="form-control font16 num required" required  id="txtMaxScore" name="txtMaxScore" placeholder="ใส่คะแนนสอบ" autocomplete="off" value="200" ></input>
                                                                </div> 
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="inputPassword3" class="col-sm-3 control-label"></label>
                                                                <div class=" col-sm-8">
                                                                    <span class="font16">- <u><b>หลังจากสร้างข้อมูลประกาศแล้ว สถานะเริ่มต้นจะเป็น "ยังไม่ประกาศผลคะแนน" หากคุณตรวจสอบแล้วว่าพร้อมประกาศผลคะแนน สามารถเข้าไปแก้ไขสถานะเป็น "ประกาศผลคะแนน"</b></u></span><br>                                                                                              
                                                                    <span class="font16">- คุณสามารถนำเข้าผลคะแนน ได้หลังจากการสร้างประกาศแล้ว</span>                                         
                                                                </div>     
                                                            </div>
                                                        </div>
                                                    </div>      

                                                    <!-- /.box-body -->
                                                    <div class="box-footer"> 
                                                        <div class="row-fluid">
                                                            <div class="col-md-8 col-sm-12 col-md-offset-3">
                                                                <button id="cmdSave" type="submit" class="btn btn-lg btn-block btn-success btn-fill"><i class="pe-7s-diskette"></i> บันทึกข้อมูล</button>     
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-footer -->
                                                    <div class="clearfix"></div>
                                             </form>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <?php
                    require_once 'footer.inc.php';
                ?>

            </div>
        </div>


    </body>

    <!--   Core JS Files   -->
    <!-- jQuery 2.2.3 -->
    <!--<script src="vendors/jquery/jquery-2.2.3.min.js"></script>-->
    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <!--<script src="assets/js/demo.js"></script>-->

    <!-- jquery validation -->
    <script src="vendors/jquery-validation/dist/jquery.validate.js"  type="text/javascript"></script>     
    <script src="vendors/jquery-validation/dist/localization/messages_th.js"  type="text/javascript"></script>    

    <!-- datepicker -->  
    <script src="vendors/jQueryCalendarThai_Ui1.11.4/jquery-ui-1.11.4.custom.js" type="text/javascript"></script>


    <script>                      
            
                // -------------------- บันทึกข้อมูล -------------------
                $.validator.setDefaults({
                    submitHandler: function() {                  
                         $('#cmdSave').prop( "disabled", true ),
                         $.ajax({
                            type: "POST",
                            url: "create-save.php",
                            data: $("#mainForm").serialize(),
                            success: function(result) {     
                                if(result.status == '1'){                                  
                                   swal("เสร็จสมบูรณ์ !!!", result.msg, "success")
                                   .then((value) =>{
                                        window.location='manage.php';
                                   })                                  
                                }else{
                                    $('#cmdSave').prop( "disabled", false),
                                    swal("ผิดพลาด", result.msg, "error");
                                }
                            }
                         });
                    }
                });       
                
                $( "#mainForm").validate({
                     ignore: [],                    
                     debug: false,
                         rules: { 
                             txtEmail:{
                                 email: true
                             }
                        }
                });           

    
                // set datepicker thai --------------------------            
                 $.datepicker.regional['th'] ={
                        changeMonth: true,
                        changeYear: true,
                        //defaultDate: GetFxupdateDate(FxRateDateAndUpdate.d[0].Day),
                        yearOffSet: 543,
                        //buttonImageOnly: true,
                        dateFormat: 'dd-mm-yy',
                        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
                        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                        constrainInput: true,
                        prevText: 'ก่อนหน้า',
                        nextText: 'ถัดไป',
                        yearRange: '-1:+5', //sohks]y
                        buttonText: 'เลือก',
                };
                $.datepicker.setDefaults($.datepicker.regional['th']);                
                $( ".selector" ).datepicker( $.datepicker.regional["th"] ); // Set ภาษาที่เรานิยามไว้ด้านบน
                $( ".selector" ).datepicker("setDate", new Date()); //Set ค่าวันปัจจุบัน
                
                // -------------------- กรอกเฉพาะตัวเลข -------------------
                $(".num").on("keypress" , function (e) {		
                    var code = e.keyCode ? e.keyCode : e.which;                   
                    if(code > 57 ){
                        return false;
                    }else if( code < 48 && code != 8){
                        return false;
                    }		
                });      
        
        </script>

</html>
