<?php

header("Content-type:application/json; charset=UTF-8");
require_once 'function.inc.php';
require_once("connections/conn.php");

//รับค่าข้อมูล
$roundno =  trim($_POST["txtRoundNo"]);
$publishdate = trim($_POST["txtPublishDate"]); //ถ้าเป็นค่าว่าง แปลว่ายังไม่ประกาศ
$status = trim($_POST["txtStatus"]); // status=0 แปลว่ายังไม่เปลี่ยนสถานะเป็น เปิดการประกาศ

if($publishdate === "" || $status == "0"){ //ไม่มีข้อมูล 
    $arr = array(
        "status"=>'0',
        "msg"=>'การสอบนี้ยังไม่ประกาศผลคะแนน กรุณาตรวจสอบ'
    );  
    echo json_encode($arr);
    exit();
}

$userid =  trim($_POST["userid"]);
$customerid =  trim($_POST["customerid"]);
$gpa =  trim($_POST["gpa"]);
if($userid == "" || $customerid == "" || $gpa == ""){ //ไม่มีข้อมูล 
    $arr = array(
        "status"=>'0',
        "msg"=>'ท่านกรอกข้อมูลไม่ครบ กรุณาตรวจสอบ'
    );  
    echo json_encode($arr);
    exit();
}


$date = date('Y-m-d');
$datetime = date('Y-m-d H:i:s');

$sql = "
    SELECT 
        *
    FROM             
        score_round, score_detail
    WHERE
        score_round.round_no=score_detail.round_no AND
        score_round.round_no=:roundno AND
        score_detail.detail_id=:userid AND
        score_detail.detail_customerid=:customerid AND
        score_detail.detail_gpa=:gpa AND
        score_round.round_status=1 
    LIMIT
        0,1
";    
$stmt = $conn->prepare($sql);
$stmt->bindParam(':roundno',$roundno);
$stmt->bindParam(':userid',$userid);
$stmt->bindParam(':customerid',$customerid);
$stmt->bindParam(':gpa',$gpa);
//$stmt->bindParam(':date',$date);
$stmt->execute();

if($stmt->rowCount() == 0){ //ถ้าระบบยังไม่มีข้อมูล
    $arr = array(
        "status"=>'0',
        "msg"=>'ไม่มีข้อมูลผู้สอบนี้ ท่านอาจกรอกข้อมูลผิด กรุณาตรวจสอบ'
    );
    echo json_encode($arr);
    exit();
    
}else{    
    
    $i = 0;    
    $tmpJson = array();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);              
    $detailno = $result['detail_no'];
    $position = $result['detail_position'];        
    $prename = $result['detail_prename'];
    $fname = $result['detail_fname'];
    $lname = $result['detail_lname'];
    $token = MD5(rand(0,1000000));
    $unpublish_date = $result['round_unpublish_date'];
    
    //เช็คว่าเกินกำหนดวันที่ประกาศแล้วหรือยัง round_unpublish_date
    if($unpublish_date < date('Y-m-d')){
        $arr = array(
            "status"=>'0',
            "msg"=>'ระบบได้ทำการปิดประกาศผลแล้ว (ประกาศครบ 90 วัน)'
        );
        echo json_encode($arr);
        exit();
    }
    
    $score = $result['detail_score'];
    $maxscore = $result['round_max_score'];
    $resultstatus = trim($result['detail_result']);
    if($resultstatus == 'ขส'){
        $resultshow = '<span style="color:orange">ขอแสดงความเสียใจ<br>ท่านสอบไม่ผ่าน</span>';
    }elseif($resultstatus == 'ตก'){
        $resultshow = '<span style="color:red">ขอแสดงความเสียใจ<br>ท่านขาดสอบ</span>';
    }elseif($resultstatus == 'ได้'){
        $resultshow = '<span style="color:green">ขอแสดงความยินดี<br>ท่านสอบผ่าน</span>';
    }
    $view = $result['detail_view']; //จำนวนการเข้าดู

    $arr = array(
        "status"=>'1',
        "msg"=>'ดึงข้อมูลสำเร็จ',
        "roundno"=>$roundno,
        "detailno"=>$detailno,
        "position"=>$position,
        "prename"=>$prename,
        "fname"=>$fname,
        "lname"=>$lname,
        "score"=>$score,
        "maxscore"=>$maxscore,
        "resultshow"=>$resultshow,
        "token"=>$token
    );
    
    $sql = "UPDATE score_detail SET
            detail_view=:view,
            detail_login_date=:datetime,
            detail_token=:token
        WHERE
            detail_no=:detailno        
    ";
    $view++;
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':view',$view);
    $stmt->bindParam(':datetime',$datetime);
    $stmt->bindParam(':token',$token);
    $stmt->bindParam(':detailno',$detailno);
    $stmt->execute();
    
    echo json_encode($arr);
    
}        



?>