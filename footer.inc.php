<input type="text" name="username_fake" value="-" tabindex="-1" aria-hidden="true" style="position: absolute; top: -999px; left:-999px; display:none;" />
<input type="password" name="password_fake" value="-" tabindex="-1" aria-hidden="true" style="position: absolute; top: -999px; left:-999px; display:none;" />

<footer class="footer">
    <div class="container-fluid">
        <nav class="pull-left">
        </nav>
        <p class="copyright pull-right">
            &copy; <script>document.write(new Date().getFullYear())</script> <a class="text-green" href="https://hr.moph.go.th/site/meeting">ระบบดูผลคะแนนสอบออนไลน์</a> กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข
        </p>
    </div>
</footer>

<!-- Sweet Alert -->
<script src="vendors/sweetalert-master/src/sweetalert-min.js"></script>    
<script src="vendors/sweetalert-master/src/sweetalert-fix-ie.js" type="text/javascript"></script>
<script>    
    function funcLogout(){
        swal({
            title: "คุณต้องการออกจากระบบ ใช่หรือไม่?",
            text: "",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            window.location = 'logout-process.php';
          } 
        });
    }        
</script>


