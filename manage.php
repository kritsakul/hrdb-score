<!doctype html>
<html lang="en">
    <head>
                
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" /> 
        <title>ระบบดูผลคะแนนสอบออนไลน์ - กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</title>

        <?php
            require_once 'redirect.inc.php';
            require_once 'cookie.inc.php';            
            require_once 'function.inc.php';
            require_once 'header.inc.php';
        ?>                

        <style>
            .header{
                padding:20px !important;                
            }
           th{
                color:#FFF !important;
                font-size:16px;
                background-color:#555 !important;
            }
            .td-middle{
                vertical-align:middle;
            }
            .text-center{
                text-align:center;
            }
            .dropdown ul li a{
                 color:#000 !important;
                 font-size:12px;
             }
             .dropdown ul li a:hover{
                 color:#990099 !important;
                 font-size:12px;
             }
             li{
                 font-size:12px !important;
             }
             .text-red{
                 color:red;
             }
            .trhover{
                background-color: #ffff99 !important;
                cursor:pointer !important;
            }
            label{
                font-size:20px !important;
                color:#000 !important;
            }                       
            a{
               color:#986eda;
            }
            table{
                width:100% !important;
            }
            table.dataTable tbody td {
                vertical-align: top;
            }
        </style>
        
    </head>
    <body>

        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">
                                
                                <div class="card" style="margin-bottom:120px;"> 
                                    
            
                                    <div class="row-fluid" style="padding-top:20px;">                                        
                                        <div class="col-md-8 col-xs-12 font24">
                                            <p style="font-size:24px; padding-left:5px;"><i class="pe-7s-note2"></i> จัดการข้อมูลประกาศผลคะแนน</p>
                                        </div>
                                        <div class="col-md-4 col-xs-12">
                                            <a href="create.php" class="btn btn-fill btn-block btn-success" style="font-size:18px;"><i class="pe-7s-plus"></i> สร้างประกาศใหม่</a>
                                        </div>                                           
                                    </div>   
                                    
                                    <div class="clearfix"></div>
                                    
                                    <div class="content" >
                                        <div class="table-responsive">                                            
                                            <form autocomplete="false">
                                                <table id="tbMain" class="table table-striped table-bordered highlight"  cellspacing="0" width="auto">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">no</th>
                                                            <th class="text-center" width='5%'>ลำดับ</th>
                                                            <th class="text-center" width='40%'>ชื่อเรื่อง</th>                                                            
                                                            <th class="text-center" width='10%'>วันที่สอบ</th>
                                                            <th class="text-center" width='10%'>วันที่ประกาศ</th>                                                            
                                                            <th class="text-center" width='10%'>เหลือเวลา (วัน)</th>
                                                            <th class="text-center" width='10%'>จำนวนผลสอบ</th>
                                                            <th class="text-center" width='10%'>สถานะ</th>
                                                            <th class="text-center" width='5%'>จัดการ</th>
                                                        </tr>                                                                    
                                                    </thead>
                                                </table>                                                    
                                            </form>
                                        </div>                     
                                    </div>
                                    
                                </div>                                

                    </div>
                </div>
            
                <?php
                    require_once 'footer.inc.php';
                ?>
      
        </div>
        
    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>

    <!-- datatables -->  
    <script src="vendors/datatable/DataTables-1.10.16/js/jquery.dataTables.js"></script>    

    <script type="text/javascript">
        
        // -----------------------------------------------------------------------------------------------------------
        //------------------------------------------- ส่วนของ datatable ------------------------------------------
        // ----------------------------------------------------------------------------------------------------------- 
        $('#tbMain').DataTable({
            "dom": 'ฺ<"top"f>Brt<"top"ilp>',
            "responsive": true,
            "ordering": false,
            "lengthMenu": [[10,  50, 100, -1], [10, 50, 100, "ทั้งหมด"]],
            "pageLength": 10,
            "processing": true,
            "language": {
                "sProcessing":   "กำลังดำเนินการ...",
	"sLengthMenu":   "แสดง _MENU_ แถว",
	"sZeroRecords":  "ไม่พบข้อมูล",
	"sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
	"sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
	"sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
	"sInfoPostFix":  "",
	"sSearch":       "ค้นหา: ",
	"sUrl":          "",
	"oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
	}
            },
            "ajax": {                        
                "url" : "manage-data.php",
                "dataSrc": ""
            },
            "columns": [
                { "data": "roundno" },  
                { "data": "no" },    
                { "data": "title" },                            
                { "data": "examdate" },
                { "data": "publishdate" },
                { "data": "datediff" },
                { "data": "total" },                
                { "data": "status" },
                { "data": "manage" },
            ],
            "columnDefs": [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
                {
                    targets: [1,3,4,5,6],
                    className: 'dt-body-center'
                }
            ]
        });
        $.fn.DataTable.ext.errMode = 'none';             

        $(document).ready(function() {
            var table = $('#tbMain').DataTable();
            $('#tbMain tbody').on( 'click', 'tr', function () {
                    var data = table.row( this ).data();   // return undefined
                    var roundno = data['roundno'];
                    window.location = 'manage-detail.php?roundno=' + roundno;
            } );  
        });
           
    </script>


    <style>        
        
            /*highlight row*/
            table.highlight tbody tr:nth-child(even):hover td{
                background-color: yellow !important;
                cursor:pointer !important;
            }

            table.highlight tbody tr:nth-child(odd):hover td {
                background-color: yellow !important;
                cursor:pointer !important;
            }        
        
            .tbMain_length{
               font-size:20px !important;
            }
         
            .dataTables_length, #tbMain_filter, .dataTables_info, #tbMain_paginate{
                font-size:18px;
            }

            .dataTables_length{
                margin-top:8px;
                margin-left:15px;
                 font-size:20px !important;
            }         

            .dataTables_filter input{       
                width:400px !important;
                font-size:16px;
            } 

            .dataTables_filter input{
                border:1px solid;              
            }            
    </style>         
        

</html>
