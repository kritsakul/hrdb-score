<?PHP


require_once 'connections/conn.php';
require_once 'cookie.inc.php';

header("Content-type:application/json; charset=UTF-8");
$tmpActID =  trim(filter_input(INPUT_POST, 'txtActID' ));
$tmpUsername =  trim(filter_input(INPUT_POST, 'txtUsername' ));
$tmpTitle =   trim(filter_input(INPUT_POST, 'txtTitle' ));
$tmpTargetGroup = trim(filter_input(INPUT_POST, 'txtTargetGroup' )); 
$tmpDesc = trim(filter_input(INPUT_POST, 'txtDesc' ));
    //ทำการตัดการขึ้นบันทัดใหม่ โดยใช้ PHP_EOL ในการค้นหา
    $tmpDesc = str_replace(PHP_EOL,'',$tmpDesc);
    
$tmpStartRegister = trim(filter_input(INPUT_POST, 'txtStartRegister' ));
$tmpStopRegister =  trim(filter_input(INPUT_POST, 'txtStopRegister' ));
$tmpDepartment = trim(filter_input(INPUT_POST, 'txtDepartment' ));
$tmpManager = trim(filter_input(INPUT_POST, 'txtManager' ));
$tmpFax = trim(filter_input(INPUT_POST, 'txtFax' ));
$tmpTel = trim(filter_input(INPUT_POST, 'txtTel' ));
$tmpEmail = trim(filter_input(INPUT_POST, 'txtEmail' ));

$tmpFood = trim(filter_input(INPUT_POST, 'selFood' ));

//สถานะ กับ หมายเหต
$tmpStatus = trim(filter_input(INPUT_POST, 'selStatus' ));
$tmpComment = trim(filter_input(INPUT_POST, 'txtComment' ));

$tmpDate = date('Y-m-d H:i:s');
$tmpError = "";

if($tmpTitle == "" || $tmpTargetGroup == "" || $tmpStartRegister == "" || $tmpStopRegister == "" 
        || $tmpDepartment == "" || $tmpManager == "" || $tmpTel == "" || $tmpUsername == "" || $tmpFood == ""){ 
        $arr = array(
            "status"=>'0',
            "msg"=>'ไม่สามารถบันทีกได้เนื่องจากกรอกข้อมูลไม่ครบ กรุณาตรวจสอบ'
        );
        echo json_encode($arr);
        exit(); 
}


//สถานะ 0ยกเลิก/ลบ 1เปิดลงทะเบียน 2ปิดลงทะเบียน 3เสร็จสมบูรณ์, 4ยังไม่เปิดให้ลงทะเบียน
//ถ้าเป็น ยกเลิก, ปิดลงทะเบียน และ ยังไม่เปิดให้ลงทะเบียน จะต้องระบุสาเหตุ
if (($tmpStatus=='5' || $tmpStatus=='2') && $tmpComment=='') {
    if($tmpStatus=='5'){
        $arr = array(
            "status"=>'0',
            "msg"=>'กรุณาระบุหมายเหตุที่ ยกเลิกการจัดประชุม'
        ) ;
    }else{
        $arr = array(
            "status"=>'0',
            "msg"=>'กรุณาระบุหมายเหตุที่ ปิดการลงทะเบียน'
        );
    }    
    echo json_encode($arr);
    exit();        
}


//หาค่า mktime เพื่อนำไปใช้เปรียบเทียบ หรือทำการแปลง
$tmpTime = mktime(0, 0, 0, gmdate("m"), gmdate("d"), gmdate("Y"));
//$tmpArrDate = explode("-", $tmpStartDate);
//$tmpStartDate2 = mktime(0, 0, 0, $tmpArrDate[1], $tmpArrDate[0], $tmpArrDate[2]);

//$tmpArrDate = explode("-", $tmpStopDate);
//$tmpStopDate2 = mktime(0, 0, 0, $tmpArrDate[1], $tmpArrDate[0], $tmpArrDate[2]);

$tmpArrDate = explode("-", $tmpStartRegister);
$tmpStartRegister2 = mktime(0, 0, 0, $tmpArrDate[1], $tmpArrDate[0], $tmpArrDate[2]);

$tmpArrDate = explode("-", $tmpStopRegister);
$tmpStopRegister2 = mktime(0, 0, 0, $tmpArrDate[1], $tmpArrDate[0], $tmpArrDate[2]);

//แปลงข้อมูลวันที่ให้อยู่ในรูปแบบ Y-m-d จะได้บันทึกใน date type ได้
//$tmpStartDate3 = date('Y-m-d', $tmpStartDate2);
//$tmpStopDate3 = date('Y-m-d', $tmpStopDate2);
$tmpStartRegister3 = date('Y-m-d', $tmpStartRegister2);
$tmpStopRegister3 = date('Y-m-d', $tmpStopRegister2);

//เช็ค วันเวลา เปิดลงทะเบียน มากกว่าวันที่ปัจจุบัน หรือไม่
/*
if ($tmpTime  > $tmpStartRegister2) {
    $arr = array(
        "status"=>'0',
        "msg"=>'มีข้อผิดพลาด คุณเปิดการลงทะเบียนย้อนหลัง วันเวลาปัจจุบัน'
    );
    echo json_encode($arr);
    exit();      
}
*/

//เช็คว่ากรอก วันที่ปิดการลงทะเบียน มากกว่า วันเริ่มต้นลงทะเบียนหรือไม่
if ($tmpStartRegister2  > $tmpStopRegister2) {
    $arr = array(
        "status"=>'0',
        "msg"=>'มีข้อผิดพลาด วันที่ ปิดการลงทะเบียน อยู่ก่อนหน้า วันที่เปิดการลงทะเบียน'
    );
    echo json_encode($arr);
    exit();      
}



$stmt = $conn->prepare("
        UPDATE meet_activity SET
            act_title = :title,
            act_date_update = :update,
            act_start_register = :startregister,
            act_stop_register = :stopregister,
            act_target_group = :targetgroup,
            act_desc = :desc,
            act_department = :department,
            act_manager = :manager,
            act_tel = :tel,
            act_fax = :fax,
            act_email = :email,
            act_food = :food,
            act_status = :status,
            act_comment = :comment
            WHERE act_id=:id        
");


$stmt->bindParam(':id', $tmpActID);
$stmt->bindParam(':title', $tmpTitle);
$stmt->bindParam(':update', $tmpDate);
$stmt->bindParam(':startregister', $tmpStartRegister3);
$stmt->bindParam(':stopregister', $tmpStopRegister3);
$stmt->bindParam(':targetgroup', $tmpTargetGroup);
$stmt->bindParam(':desc', $tmpDesc);
$stmt->bindParam(':department', $tmpDepartment);
$stmt->bindParam(':manager', $tmpManager);
$stmt->bindParam(':tel', $tmpTel);
$stmt->bindParam(':fax', $tmpFax);
$stmt->bindParam(':email', $tmpEmail);
$stmt->bindParam(':food', $tmpFood);
$stmt->bindParam(':status', $tmpStatus);
$stmt->bindParam(':comment', $tmpComment);
$stmt->execute();
 
 $arr = array(
    "status"=>'1',
    "msg"=>'ทำการแก้ไขข้อมูลการประชุม เรียบร้อย',
    "error"=>$tmpError
);

echo json_encode($arr);

?>