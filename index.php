<!doctype html>
<html lang="en">
    <head>
        
        <?php
        
            require_once 'connections/conn.php';
            require_once 'function.inc.php';
            
            //เช็คข้อมูล ประกาศนี้มีข้อมูลหรือไม่
            $roundno = filter_input(INPUT_GET, 'roundno');
            $sql = "SELECT * FROM score_round WHERE round_no=:roundno LIMIT 0,1";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':roundno', $roundno);
            $stmt->execute();

            //ถ้าไม่มีข้อมูล actid จะไม่ redirect ไปหน้าหลัก
            if ($stmt->rowCount() <= 0) {
                //header("Location: https://moph.thaijobjob.com/"); /* Redirect browser */
                header("Location: login.php"); /* Redirect browser */
                exit();
                
            } else {                
                $result = $stmt->fetch(PDO::FETCH_ASSOC);                
                $roundno = $result['round_no'];
                $title = $result['round_title'];
                $examdate = $result['round_exam_date'];
                $maxscore = $result['round_max_score'];
                $publishdate = trim($result['round_publish_date']);
                $unpublishdate = trim($result['round_unpublish_date']);
                if($publishdate == ""){
                    $fulldate = "ยังไม่ประกาศผล";
                }else{
                    $datediff = dateDifference(date('Y-m-d'), $unpublishdate );                          
                    $fulldate = 'ประกาศผลตั้งแต่ ' . shortthaidate($publishdate, 'full') . " ถึง " . shortthaidate($unpublishdate,  'full');
                }

                $status = $result['round_status'];
                if($result['round_status'] == 0){
                    $statusshow = '<span style="color:red">ยังไม่ประกาศ</span>';
                }elseif($result['round_status'] == 1){   
                    $statusshow = '<span style="color:green">เปิดประกาศผล</span>';      
                }          
                
            }      
            
        ?>
        
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />        
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <title>ระบบดูผลคะแนนออนไลน์ - กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</title>

        <!--===============================================================================================-->	
        <link rel="stylesheet" type="text/css" href="Login_v1/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="Login_v1/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="Login_v1/vendor/animate/animate.css">	
        <link rel="stylesheet" type="text/css" href="Login_v1/vendor/css-hamburgers/hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="Login_v1/vendor/select2/select2.min.css">
        <link rel="stylesheet" type="text/css" href="Login_v1/css/util.css">
        <link rel="stylesheet" type="text/css" href="Login_v1/css/main.css">
        <link href="vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
        <style>
            .hide{
                visibility: hidden;
                float:left;
                position:fixed;
                left:-99999999px;
                top:-99999999px;
            }
            .pointer{
                cursor:pointer !important;
            }
                        
            body, p, div, span, button, a, td, label{
                font-family: 'Prompt', sans-serif !important;
            }
        </style>
        <!--===============================================================================================-->    
        
    </head>
    
    <body>

        <div class="limiter">
                <div class="container-login100">
                        <div class="wrap-login100" style="text-align:center;">
                            <div class="js-tilt" data-tilt style="margin:10px auto 30px auto; width: 316px;">
                                <img src="Login_v1/images/moph-logo.png" alt="IMG" style="width:90%;">
                                </div>

                                <form class="login100-form validate-form" id="mainForm" name="mainForm">
                                    
                                    
                                        <input type="hidden" id="txtRounNo" name="txtRoundNo" value="<?php echo $roundno ?>">
                                        <input type="hidden" id="txtPublishDate" name="txtPublishDate" value="<?php echo $publishdate ?>">
                                        <input type="hidden" id="txtStatus" name="txtStatus" value="<?php echo $status ?>">
                                        <input type="hidden" id="txtDetailNo" name="txtDetailNo" value="">
                                        
                                        <span class="login100-form-title" style="padding-bottom:10px; font-size:28px;">
                                            <b>ระบบดูผลคะแนนสอบออนไลน์</b>
                                        </span>

                                        <div class="text-center" style="padding-bottom:30px;">
                                            <?php
                                                echo '<b>'.$title.'</b>';
                                                echo '<br><span style="font-size:16px;">(สอบ'. formaldate($examdate) .')</span>';
                                            ?>
                                        </div>
                      
                                            <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">                                                
                                                <input class="input100" type="text" required name="userid" id="userid" placeholder="รหัสประจำตัวสอบ" maxlength="15" value="261399999" >
                                                    <span class="focus-input100"></span>
                                                    <span class="symbol-input100">
                                                            <i class="fa fa-address-card-o" aria-hidden="true"></i>
                                                    </span>
                                            </div>

                                            <div class="wrap-input100 validate-input" data-validate = "Password is required">
                                                    <input class="input100" type="text" required id="customerid" name="customerid" maxlength="13" value="1234567891011" placeholder="รหัสประจำตัวประชาชน 13 หลัก">
                                                    <span class="focus-input100"></span>
                                                    <span class="symbol-input100">
                                                            <i class="fa fa-key" aria-hidden="true"></i>
                                                    </span>
                                            </div>

                                            <div class="wrap-input100 validate-input" data-validate = "Password is required" >
                                                    <input class="input100" type="text" id="gpa" required name="gpa" maxlength="5"  value="3.59" placeholder="เกรดเฉลี่ย (ที่ใช้ตอนสมัคร)">
                                                    <span class="focus-input100"></span>
                                                    <span class="symbol-input100">
                                                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                                    </span>
                                            </div>                 

                                            <div id="divConfirm" class="container-login100-form-btn" style="padding-top:5px;">
                                                <a id="btnResult" name="btnResult" style="color:#FFF; font-size:20px;" class="login100-form-btn pointer"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;ดูผลคะแนน</a>
                                            </div>
                                            <div id="divCancel" class="container-login100-form-btn hide" style="padding-top:5px;">
                                                <a href="./?roundno=<?php echo $roundno ?>" style="color:#FFF; background-color:red; font-size:20px;" class="login100-form-btn pointer"><i class="fa fa-times-circle" aria-hidden="true"></i>&nbsp;ยกเลิกการดู</a>
                                            </div>
                                                          
                                            <div style="font-size:14px; margin-top:10px; text-align:center;">
                                                <?php echo $fulldate ?>
                                            </div>                                    
                                    
                                            <div id="divShowResult" class="wrap-input100 validate-input hide" style="text-align:center !important; margin-top:20px; border:1.5px solid green; padding:15px 10px; border-radius: 15px;">
                                                <div style="font-size:24px; font-weight:bold;" id="divResult" name="divResult"></div>
                                                <div style="font-size:22px; font-weight:bold; color:blue;" id="divScore" name="divScore"></div>
                                                <div style="font-size:14px; font-weight:bold;" id="divMax" name="divMax"></div>
                                                <br>
                                                <div style="font-size:18px; font-weight:bold;" id="divName" name="divName"></div>
                                                <div style="font-size:16px;" id="divPosition" name="divPosition"></div>
                                                <div style="font-size:16px;" id="divPrint" name="divPrint"></div>                                                
                                            </div>                                          

                                            <div class="text-center p-t-18">
                                                <p>กลุ่มงานสรรหาบุคคล</p>
                                                <p style="font-size:14px;">กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</p>
                                                <p>ติดต่อเรา 02-590-1346</p>
                                                <a target="_blank" href="login.php" style="color:green;"><i class="fa fa-info-circle" aria-hidden="true" ></i> <u>สำหรับผู้ดูแลระบบ</u></a>                                                
                                            </div>
                                    
                                </form>
                        </div>
                </div>
        </div>
        
    </body>

    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="Login_v1/vendor/bootstrap/js/popper.js"></script>
    <script src="Login_v1/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="Login_v1/vendor/select2/select2.min.js"></script>
    <script src="Login_v1/vendor/tilt/tilt.jquery.min.js"></script>
    <script >
            $('.js-tilt').tilt({
                    scale: 1.1
            })
    </script>
    <script src="Login_v1/js/main.js"></script>

    <!-- jquery validation -->
    <script src="vendors/jquery-validation/dist/jquery.validate.js"  type="text/javascript"></script>     
    <script src="vendors/jquery-validation/dist/localization/messages_th.js"  type="text/javascript"></script>    

    <script type="text/javascript" src="vendors/jquery-loading-overlay-master/dist/loadingoverlay.js"></script>
    
    <!-- Sweet Alert -->
    <script src="vendors/sweetalert-master/src/sweetalert-min.js"></script>    
    <script src="vendors/sweetalert-master/src/sweetalert-fix-ie.js" type="text/javascript"></script>
    
    <script type="text/javascript">                

        $("#btnResult").click(function(){         
            $.LoadingOverlay("show");
            $.ajax({
                type: "POST",
                url: "index-result.php",
                data: $("#mainForm").serialize(),
                success: function(result) {                     
                    if(result.status == '1'){
                        setTimeout(function(){
                            $.LoadingOverlay("hide");
                            $('#divShowResult').removeClass('hide');
                            $('#divCancel').removeClass('hide');
                            $('#divConfirm').addClass('hide');

                            $('#divResult').html(result.resultshow);
                            $('#divScore').html('คะแนนที่ได้ ' + result.score + ' คะแนน');
                            $('#divMax').html('(คะแนนเต็ม ' + result.maxscore + ' คะแนน)');
                            $('#divName').html(result.prename + result.fname + ' ' + result.lname);
                            $('#divPosition').html('ตำแหน่ง' + result.position);
                            $('#txtDetailNo').val(result.detailno);
                            $('#divPrint').html('<a target="_blank" href="pdf.php?roundno='+result.roundno+'&detailno='+result.detailno+'&token='+result.token+'" style="color:green;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> <u>พิมพ์ผลการสอบ (pdf)</u></a>');
                        }, 1500);

                    }else{
                        $.LoadingOverlay("hide");
                        swal("ผิดพลาด", result.msg, "error");
                        $('#divShowResult').addClass('hide');
                        $('#divCancel').addClass('hide');
                        $('#divConfirm').deleteClass('hide');
                        $('#divResult').html('');
                        $('#divScore').html('');
                        $('#divMax').html('');
                        
                    };
                }
            });       
        });            

    </script>

</html>
