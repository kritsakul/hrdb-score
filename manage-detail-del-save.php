<?PHP

header("Content-type:application/json; charset=UTF-8");
require_once 'redirect.inc.php';
require_once 'cookie.inc.php';
require_once 'function.inc.php';

$roundno =  trim(filter_input(INPUT_POST, 'txtRoundNoDel' ));
$password =  trim(filter_input(INPUT_POST, 'txtConfirmPass' ));
$tmpMD5 = md5($password);
$tmpDate = date('Y-m-d H:i:s');
$tmpError = "";

if($roundno == "" || $password == "" || $tmpMainUsername == ""){    
   $arr = array(
       "status"=>'0',
       "msg"=>'ไม่สามารถลบได้เนื่องจากกรอกข้อมูลไม่ครบ กรุณาตรวจสอบ'
   );
   echo json_encode($arr);
   exit(); 
}


$stmt = $conn->prepare("SELECT * FROM user WHERE username=:username AND password=:md5");
$stmt->bindParam(':username', $tmpMainUsername);
$stmt->bindParam(':md5', $tmpMD5);
$stmt->execute();
$dataStmt = $stmt->fetch(PDO::FETCH_ASSOC);
$tmpCount = $stmt->rowCount();
   
if($tmpCount == 0){ //ไม่มีข้อมูล
    $arr = array(
        "status"=>'0',
        "msg"=>'รหัสผ่านผิดพลาด กรุณาตรวจสอบ'
    );
    echo json_encode($arr);
    exit();
}

//ลบข้อมูลประกาศ
$stmt = $conn->prepare("DELETE FROM score_round  WHERE round_no=:roundno");
$stmt->bindParam(':roundno', $roundno);
$stmt->execute();

//ลบข้อมูลประกาศรายละเอียด
$stmt = $conn->prepare("DELETE FROM score_detail  WHERE round_no=:roundno");
$stmt->bindParam(':roundno', $roundno);
$stmt->execute();


$arr = array(
    "status"=>'1',
    "msg"=>'ทำการลบข้อมูล เสร็จเรียบร้อย',
    "error"=>$tmpError
);

echo json_encode($arr);

?>