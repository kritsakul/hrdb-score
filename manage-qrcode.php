<!doctype html>
<html lang="en">
    <head>
        
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />        
        <title>สร้าง qr-code กับ ลิงก์ - ระบบดูผลคะแนนสอบออนไลน์ กองบริหารทรัพยากรบุคคล สป.</title>

        <?php
            require_once 'connections/conn.php';
            require_once 'cookie.inc.php';
            require_once 'redirect.inc.php';
            require_once 'function.inc.php';            
            require_once 'header.inc.php';        
            
            $tmpRoundNo = filter_input(INPUT_GET, 'roundno');             
            
            //เช็คข้อมูล ประกาศนี้มีข้อมูลหรือไม่
            $roundno = filter_input(INPUT_GET, 'roundno');
            $sql = "SELECT * FROM score_round WHERE round_no=:roundno LIMIT 0,1";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':roundno', $roundno);
            $stmt->execute();

            if ($stmt->rowCount() <= 0) {
                header("Location: index.php"); /* Redirect browser */
                exit();
            } else {
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
            } 
        
            $title = $result['round_title'];
            
        ?>

        <style>
            .header{
                padding:20px !important;
            }
           th{
                font-size:16px;
            }
            .font16 .control-label{
                font-size:16px;
                color:#000;
            }
            .font18{
                font-size:18px;
            }
            ::placeholder {
                color:red;
            }
            .error{
                color:#FF0000 !important;
            }
            input, select{
                border:1px solid #AAA !important;
            }
            .select2-container .select2-selection--single {
                    height:35px;
            }
        </style>
        
    </head>
    <body>
        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                    require_once 'vendors/phpqrcode/qrlib.php'; 
                ?>

                <div class="content">
                    <div class="container-fluid">
                        
                        <div class="row">                            
                            <div class="col-md-12">
                                
                                <div class="card">    
                                    
                                    <p style="font-size:24px; padding:20px 10px 0px 20px;">
                                        <i class="pe-7s-note2"></i> <u><a href="manage.php" style="color:green;"> จัดการข้อมูลประกาศผลคะแนน</a></u> > 
                                        <u><a href="manage-detail.php?roundno=<?php echo $roundno ?>" style="color:green;"> จัดการข้อมูล</a></u> > 
                                        สร้าง qr-code กับ short link
                                    </p>                                    

                                    <div class="content">
                                        <form class="form-horizontal">  
                                                <div class="form-group">  
                                                    
                                                    <div class="col-xs-12" style="text-align:center;">
                                                        <p style="font-size:24px;"><?php echo $title ?></p>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12" style="text-align:center; font-size:18px;">      
                                                            <p class="label label-default">QR code</p><br>
                                                            <?php
                                                                $tmpUrl = 'https://hr.moph.go.th/site/score/?roundno=' . $roundno;
                                                            ?>
                                                            <img src="create-qrcode.php?text=<?php echo $tmpUrl ?>">                                                         
                                                        </div>                                   

                                                        <div class="col-md-6 col-sm-12" style="text-align:center; font-size:18px;">
                                                            <p class="label label-default">short link</p><br>
                                                            <?php
                                                                //check ว่ามีข้อมูล shortlink หรือไม่                                                        
                                                                require_once('vendors/bitlyphp/bitly.php');
                                                                $client_id = '2f4b1eb5beb9b615ac70b16e34daf4bc6b899112';
                                                                $client_secret = '149e58cfb3f5d508386822be40afc8ce75ca1c26';
                                                                $user_access_token = '4086ebb73f8ed9e66f0ec1e0f7926ae4fcbd3188';

                                                                $tmpUrlPrefix = 'https://hr.moph.go.th/site/score';
                                                                $longUrl = $tmpUrlPrefix . "/?roundno=" . $roundno;

                                                                $params = array();
                                                                $params['access_token'] = $user_access_token;
                                                                $params['longUrl'] = $longUrl;
                                                                //$params['domain'] = 'hr.meeting';
                                                                $results = bitly_get('shorten', $params);
                                                                $tmpShortUrl = $results['data']['url'];  
                                                                //var_dump($results);
                                                                //print_r($results);                                                                                       
                                                            ?>
                                                            <a target="_blank" href="<?php echo $tmpShortUrl ?>"><?php echo $tmpShortUrl ?></a>
                                                        </div>
                                                    </div>
                                                        
                                                </div>
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <?php
                    require_once 'footer.inc.php';                    
                ?>

        </div>
    </body>
    
    <!--   Core JS Files   -->
    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
    <script src="assets/js/demo.js"></script>
    <script src="script.js" type="text/javascript"></script>           
 
</html>
