<?php

/** Error reporting */
header("Content-type:application/json; charset=UTF-8");

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

//require_once 'connections/conn.php';
require_once 'vendors/PHPExcel-1.8/Classes/PHPExcel.php';
require_once 'vendors/PHPExcel-1.8/Classes/PHPExcel/IOFactory.php';

$files = $_FILES['file'];
$filetmp = $_FILES['file']['tmp_name'];
$filename = $_FILES['file']['name'];
$fileinfo = pathinfo($_FILES['file']['name']);
$temporary = explode(".", $_FILES["file"]["name"]);
$allow_file = array("xls", "xlsx");

if(isset($_FILES['file']['name']) && isset($_FILES['file']['tmp_name']) AND $filename !== ""){
    
    if(in_array($fileinfo['extension'], $allow_file) && $filetmp !== ""){
        
        $inputFileType = PHPExcel_IOFactory::identify($filetmp);  
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);  
        $objReader->setReadDataOnly(true);  
        $objPHPExcel = $objReader->load($filetmp);  
        
        $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
        $highestRow = $objWorksheet->getHighestRow();
        $highestColumn = $objWorksheet->getHighestColumn();

        $headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
        $headingsArray = $headingsArray[1];

        $r = -1;
        $namedDataArray = array();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
            if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                ++$r;
                foreach($headingsArray as $columnKey => $columnHeading) {
                    $namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                }
            }
        }
        
        $arrjson = array();
        $i=0;
        foreach ($namedDataArray as $result) {

            $resultshow = $result["Result"];
            if($resultshow == '0' || $resultshow == '' ){
                $resultshow = "ขส";
            }
            
            //if คะแนนเป็น ขส. ระบบจะให้คะแนนเป็น 0
            if(trim($result["Score"]) == 'ขส'){
                $score = 0;
            }else{
                $score = $result["Score"];                
            }

            $arrjson[$i]["id"] = $result["ID"];
            $arrjson[$i]["position"] = $result["Position"];
            $arrjson[$i]["prename"] = $result["Name1"];
            $arrjson[$i]["fname"] = $result["Name2"];
            $arrjson[$i]["lname"] = $result["Name3"];
            $arrjson[$i]["customerid"] = $result["CustomerID"];
            $arrjson[$i]["gpa"] = $result["GPA"];
            $arrjson[$i]["score"] = $score;
            $arrjson[$i]["result"] = $resultshow;
            $i++;            

        } //END FOREACH

        $arr = array(
            "status"=>'1',
            "msg"=>'ดำเนินการเรียบร้อย',
            "json"=>$arrjson
        );
        echo json_encode($arr);
        exit();
        
    }else{
        //$name = $_FILES["fileImport"]["name"];
        $arr = array(
            "status"=>'0',
            "msg"=>'ระบบสามารถอ่านได้เฉพาะไฟล์ Excel เท่านั้น (*.xls, *.xlsx)'
        );
        echo json_encode($arr);
        exit();

    }
    
}else{
    
    //$name = $_FILES["fileImport"]["name"];
    $arr = array(
        "status"=>'0',
        "msg"=>'มีบางอย่างผิดพลาด ไม่สามารถอ่านข้อมูลได้ กรุณาตรวจสอบ นามสกุลไฟล์ถูกต้องหรือไม่, มีชื่อคอลัมน์ ตรงตามแบบฟอร์มหรือไม่'
    );
    echo json_encode($arr);
    exit();

}


//$name = $_FILES["fileImport"]["name"];
$arr = array(
    "status"=>'0',
    "msg"=>'มีบางอย่างผิดพลาด ไม่สามารถอ่านข้อมูลได้ กรุณาตรวจสอบ นามสกุลไฟล์ถูกต้องหรือไม่, มีชื่อคอลัมน์ ตรงตามแบบฟอร์มหรือไม่'
);

echo json_encode($arr);

