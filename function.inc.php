<?php

    $thaimontharr = array(
        "01" => "มกราคม",
        "02" => "กุมภาพันธ์",
        "03" => "มีนาคม",
        "04" => "เมษายน",
        "05" => "พฤษภาคม",
        "06" => "มิถุนายน",
        "07" => "กรกฎาคม",
        "08" => "สิงหาคม",
        "09" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    );

    $thaishortmontharr = array(
            "01" => "ม.ค.",
            "02" => "ก.พ.",
            "03" => "มี.ค.",
            "04" => "เม.ย.",
            "05" => "พ.ค.",
            "06" => "มิ.ย.",
            "07" => "ก.ค.",
            "08" => "ส.ค.",
            "09" => "ก.ย.",
            "10" => "ต.ค.",
            "11" => "พ.ย.",
            "12" => "ธ.ค."
     );      

    $thaiday = array ( "อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์" );
    $thaishortday = array ( "อา.", "จ.", "อ.", "พ.", "พฤ.", "ศ.", "ส." );

        
    function fullthaidate($date, $type = NULL){ //full, medium, short
        global $thaimontharr;
        global $thaishortmontharr;
        global $thaiday;
        global $thaishortday;        
        $strtotime = strtotime($date);
        $week = date("w", $strtotime);
        
        $year = date("Y",$strtotime);
            $tmpthaiyear = $year + 543;
        $month = date("m",$strtotime);
        $date = date("d",$strtotime);
                        
        if($type == 'full'){
            return $thaiday[$week] . " " . $date . " " . $thaimontharr[$month] . " " . $tmpthaiyear;
        }else{
            return $thaishortday[$week] . " " . $date . " " . $thaishortmontharr[$month] . " " . $tmpthaiyear;
        }
    }
    
    //return 12 ธ.ค. 62 หรือ 12 ธันวาคม 2562
    function shortthaidate($date, $type = NULL){ //full, medium, short
        global $thaimontharr;
        global $thaishortmontharr;
        global $thaiday;
        global $thaishortday;        
        $strtotime = strtotime($date);
        $week = date("w", $strtotime);
        
        $year = date("Y",$strtotime);
            $tmpthaiyear = $year + 543;
        $month = date("m",$strtotime);
        $date = date("d",$strtotime);
                        
        if($type == 'full'){
            return $date . " " . $thaishortmontharr[$month] . " " .  $tmpthaiyear;
        }else{
            return $date . " " . $thaishortmontharr[$month] . " " . substr($tmpthaiyear,2);
        }
    }

    function shortdate($date){
        global $thaimontharr;
        global $thaishortmontharr;
        global $thaiday;
        global $thaishortday;   
        $arr = explode("-",$date);
        $strtotime = strtotime($date);
        $date = date_create($date);        
        $week = date("w", $strtotime);        
        $day = date('d',$strtotime);
        $month = date('m',$strtotime);
        $year = date('Y',$strtotime) + 543;
        
        //$tmpthaiyear = intval($arr[0]) + 543;
        //return $arr[2] . "/" . $arr[1] . "/" . substr($tmpthaiyear,2);        
        return $day . "/" . $month . "/" . substr($year,2); 
    }

    //31-12-61 12:10:20
    //return 12 ธ.ค. 62 หรือ 12 ธันวาคม 2562
    function shortdatetime($date){ //$date = Y-m-d H:i:s
        if(trim($date) == ""){
            return "";
            exit();
        }
        global $thaimontharr;
        global $thaishortmontharr;
        global $thaiday;
        global $thaishortday;        
        $strtotime = strtotime($date);
        
        $year = date("Y",$strtotime);
            $tmpthaiyear = $year + 543;
        $month = date("m",$strtotime);
        $date = date("d",$strtotime);
        $hour = date("H",$strtotime);
        $minute = date("i",$strtotime);
        $second = date("s",$strtotime);         
        return $date . "-" . $month . "-" .  substr($tmpthaiyear,2) . " " . $hour . ":" . $minute;        
    }    
    
    function fullthaidatetime($date, $type = NULL){      
        global $thaimontharr;
        global $thaishortmontharr;
        global $thaiday;
        global $thaishortday;
        
        $strtotime = strtotime($date);
        $week = date("w", $strtotime);
        $arr = explode("-",$date);
        $tmpthaiyear = intval($arr[0]) + 543;
        
        //แยกวันที่ กับ เวลา
        $arrday = explode(" ", $arr[2]);
        $day = $arrday[0];
        $time =  $arrday[1];
        if($type == 'full'){
             return $thaiday[$week] . " " . $day . " " . $thaimontharr[$arr[1]] . " " . $tmpthaiyear . " เวลา " . $time . " น.";
        }else{
             return $thaishortday[$week] . " " . $day . " " . $thaishortmontharr[$arr[1]] . " " . $tmpthaiyear . " เวลา " . $time . " น.";
        }       
    }    
    
    //วันที่ ........ เดือน ................... พ.ศ. ...................
    function formaldate($date){              
        global $thaimontharr;        
        $datetotime = strtotime($date);
        return "วันที่ " . date('j', $datetotime) . '  เดือน ' . $thaimontharr[date('m', $datetotime)] . ' พ.ศ. ' . (intval(date('Y', $datetotime)) + 543);       
    }    
    
    //datechrisformat
    //25-12-2562 แปลงเป็น 2018-12-25 
    function datethaitointer($date){
        $arr = explode("-",$date);
        return (intval($arr[2])-543) . "-" . $arr[1] . "-" . $arr[0];
    }
    
    //2018-12-25  แปลงเป็น 25-12-2562  
    function dateintertothai($date){
        $datetotime = strtotime($date);
        return date('d', $datetotime)  . "-" . date('m', $datetotime) . "-" . (intval(date('Y', $datetotime))+543) ;
    }
    
    function convertAgeWork($date_1)
    {
        $date_2 = date("Y-m-d");                        
        $date1=date_create($date_1);
        $date2=date_create($date_2);                            
        $diff=date_diff($date1,$date2);
        if($diff->format("%Y") > 0){
            return $diff->format("%yปี %mเดือน %dวัน");     
        }else{
            return $diff->format("%mเดือน %dวัน");     
        }
    }

    function convertYearWork($date_1)
    {
        $date_2 = date("Y-m-d");                        
        $date1=date_create($date_1);
        $date2=date_create($date_2);                            
        $diff=date_diff($date1,$date2);
        return $diff->format("%y");     
    }        
    
    function convertMonthWork($date_1)
    {
        $date_2 = date("Y-m-d");                        
        $date1=date_create($date_1);
        $date2=date_create($date_2);                            
        $diff=date_diff($date1,$date2);
        return $diff->format("%m");     
    }    
    
    function convertDayWork($date_1)
    {
        $date_2 = date("Y-m-d");                        
        $date1=date_create($date_1);
        $date2=date_create($date_2);                            
        $diff=date_diff($date1,$date2);
        return $diff->format("%d");     
    }    
    
    //หาปีงบประมาณ YYYY-MM-DD คศ
    function calYearBudget($string)
    {
        $arrDate = explode("-",$string);
        $year = $arrDate[0] + 543;
        $month = $arrDate[1];
        $day = $arrDate[2];

        if($month >= 10 AND $day >= 1){
            return $year + 1;
        }else{
            return $year;
        }            
    }    

    //หาว่าครบ 6 เดือน ปีงบประมาณใด
    function calYearBudget6Month($string)
    {
        $date = date("Y-m-d", strtotime("+6 months", strtotime($string)));
        return calYearBudget($date);
    }    
        
    //หาว่าครบ 1 เดือน ปีงบประมาณใด
    function calYearBudget10Year($string)
    {
        $date = date("Y-m-d", strtotime("+10 years", strtotime($string)));
        return calYearBudget($date);
    }       
        
    //นับจำนวน วันในเดือน
    function countDateInMonth($month, $year) //year=พ.ศ.
    {
        $year = $year-543;
        $time = mktime(0,0,0,$month,0,$year);
        return date("t",$time);
    }
    
    //หาปีงบประมาณ
    function calCheckYearBudget($month, $year) //year=พ.ศ.
    {
        if($month >= 10){
            return $year+1;
        }else{
            return $year;
        }
    }
    
    //หาจำนวนวัน
    //format date1 = 2018-12-01 08:30:00 หรอ 2018-12-01
   function leavedatediff($date1, $date2){
        //ช่วงเช้า 08.30-12.30 return 0.5
        //ช่วงบ่าย 12.30-16.30 return 0.5
        //เต็มวัน 08.30-16.30 return 1.0
        $test1 = new DateTime($date1);
        $test2 = new DateTime($date2);
        $datediff = $test2->diff($test1);
        $days = $datediff->format('%a');
        $hour = $datediff->format('%h');
        if($days > 0){ //ถ้าจำนวนวันมากกว่า 1 วันขึ้นไป จะต้องนำมา คูณ 8 
            $hour = $hour+($days*8);
        }
        return $hour/8;
   }
   
   
    function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
        $datediff = $datetime2->diff($datetime1);
        $interval = date_diff($datetime2, $datetime1);
        if($interval->invert !== 0){ //ถ้าหมดอายุจะเท่ากับ 0
            return $interval->format($differenceFormat);
        }else{
            return 0;
        }
    }
    
?>