<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <title>ระบบประกาศผลคะแนนออนไลน์ - กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</title>

        <?php            
            require_once 'cookie.inc.php';            
            require_once 'header.inc.php';
            if(trim($tmpMainUsername) !== ""){
               header('Location: manage.php');
            }            
        ?>
        
        <style>
            .header{
                padding:20px !important;
            }
           th{
                color:#FFF;
                font-size:16px;
            }
        </style>
        
    </head>
    <body>

        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">
                        
                        <div class="row">                            
                            <div class="col-md-12">
                               
                                <div class="row">
                                        
                                    <div class="col-md-6 col-md-offset-3">
                                        
                                            <div class="card col-offset-3" style="padding-bottom:15px;" >
                                                <div class="header" style="text-align:center;">
                                                    <i class="pe-7s-unlock"></i>
                                                    <h4 class="title">ผู้ดูแลระบบ</h4>
                                                </div>
                                                <div class="content">
                                                    <form id="mainForm">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label style="color:#000;">ชื่อผู้ใช้งาน / Username</label>
                                                                    <input type="text" id="username" name="val-username" class="form-control" placeholder="Username" value="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <label style="color:#000;">รหัสผ่าน / Password</label>
                                                                    <input type="password" id="password" name="val-password" class="form-control" placeholder="Password" value="">
                                                                </div>
                                                            </div>
                                                        </div>      
                                                        <br>

                                                        <button type="submit" class="btn btn-block btn-fill btn-success" style="font-size:18px;"><i class="pe-7s-key"></i> เข้าสู่ระบบ</button>
                                                        <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        
                                    </div>
                               
                            </div>
                        </div>
                        
                    </div>
                </div>


                <?php
                    require_once 'footer.inc.php';
                ?>

            </div>
        </div>


    </body>


    <!-- All Jquery -->
    <script src="vendors/jquery/jquery-2.2.3.min.js"></script>

    <!-- jquery validation -->
   <script src="vendors/jquery-validation/dist/jquery.validate.js"  type="text/javascript"></script>     
   <script src="vendors/jquery-validation/dist/localization/messages_th.js"  type="text/javascript"></script>      

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>   
   <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="assets/js/demo.js"></script>
       
    
    <script type="text/javascript">                
            
            $.validator.setDefaults({
                submitHandler: function() {
                     funcLogin();
                }
            });                
            $( "#mainForm").validate();        

            $('.login').keypress(function (e){
                 if(e.which == 13){
                     funcLogin();
                 }
             });               
                                  
            function funcLogin(){
                $.ajax({
                    type: "POST",
                    url: "login-process.php",
                    data: $("#mainForm").serialize(),
                    success: function(result) {                        
                        if(result.status == '1'){
                            window.location='manage.php'
                        }else{
                            swal("ผิดพลาด", result.msg, "error");
                        };
                    }
                });
            }                       

    </script>


    
</html>
