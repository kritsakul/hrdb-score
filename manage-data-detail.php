<?php
    
require_once 'redirect.inc.php';
require_once 'function.inc.php';

$tmpJson = array();
$roundno = $_POST['roundno'];

//นับจำนวนผู้เข้าสอบ
$sql = "
    SELECT
       *
    FROM
        score_detail
    WHERE 
        round_no=:roundno
     ORDER BY  
        detail_id ASC
";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':roundno',$roundno);
$stmt->execute();

$i=0;
while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {        
              
        $detailno = $result['detail_no'];
        $detailid = $result['detail_id'];
        $position = $result['detail_position'];
        $prename = $result['detail_prename'];
        $fname = $result['detail_fname'];
        $lname = $result['detail_lname'];
        $customerid = $result['detail_customerid'];
        $gpa = $result['detail_gpa'];
        $score = $result['detail_score'];
        $resultdata = $result['detail_result'];
        $logindate = $result['detail_login_date'];
        $view = $result['detail_view'];
        
        $tmpJson[$i]['no'] = $i+1;
        //$tmpJson[$i]['detail_no'] = $detailno;
        $tmpJson[$i]['detail_id'] = $detailid; 
        $tmpJson[$i]['position'] = $position;   
        $tmpJson[$i]['name'] = $prename . $fname . ' ' . $lname;   
        $tmpJson[$i]['customerid'] = $customerid;   
        $tmpJson[$i]['gpa'] =$gpa;  
        $tmpJson[$i]['score'] =$score;  
        $tmpJson[$i]['result'] =$resultdata;  
        $tmpJson[$i]['logindate'] ='<span class="font12">'.shortdatetime($logindate).'</span>';  
        $tmpJson[$i]['view'] =$view;  
        
        $i++;                                         
}

//print_r($tmpJson);
echo json_encode($tmpJson);


?>