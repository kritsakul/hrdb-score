<?php

//header("Content-type:text-plain; charset=UTF-8");
error_reporting(E_ALL); 
ini_set('display_errors', 1);

require_once("connections/conn.php");
require_once("function.inc.php");
require_once 'vendors\mpdf-master\mpdf.php';

$roundno = trim($_GET["roundno"]);
$detailno = trim($_GET["detailno"]);
$token = trim($_GET["token"]);
$date=date('Y-m-d');

$sql = "
    SELECT 
        *
    FROM             
        score_round, score_detail
    WHERE
        score_round.round_no=score_detail.round_no AND
        score_round.round_no=:roundno AND
        score_detail.detail_no=:detailno AND
        score_detail.detail_token=:token AND
        score_round.round_unpublish_date>=:date
    LIMIT
        0,1
";    
$stmt = $conn->prepare($sql);
$stmt->bindParam(':roundno',$roundno);
$stmt->bindParam(':detailno',$detailno);
$stmt->bindParam(':token',$token);
$stmt->bindParam(':date',$date);
$stmt->execute();

if($stmt->rowCount() == 0){
    echo "ไมมีข้อมูล กรุณาตรวจสอบ";
    exit();
    
}else{
    $result = $stmt->fetch(PDO::FETCH_ASSOC);    
    $name = $result['detail_prename'] . $result['detail_fname'] . ' ' . $result['detail_lname'];
    
    $resultstatus = trim($result['detail_result']);
    if($resultstatus == 'ขส'){
        $resultshow = 'ขาดสอบ';
    }elseif($resultstatus == 'ตก'){
        $resultshow = 'ไม่ผ่าน';
    }elseif($resultstatus == 'ได้'){
        $resultshow = 'ผ่าน';
    }
    
}



//ใช้ font sarabun
/*
$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'orientation' => 'P',
    'format' => 'A4',
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/custom/font/directory',
    ]),
    'fontdata' => $fontData + [
        'sarabun' => [
            'R' => 'THSarabunNew.ttf',
            'I' => 'THSarabunNew Italic.ttf',
            'B' => 'THSarabunNew Bold.ttf',
            'BI' => 'THSarabunNew BoldItalic.ttf',
        ]
    ],
    'default_font' => 'sarabun'
]);
$mpdf->adjustFontDescLineheight = 1;
*/

ob_start();

?>

<style>
    
    @page {
        margin-top:1.5cm;
        margin-right:1.3cm;
        margin-bottom:1cm;
        margin-left:2.5cm;
    }
    p.tab{margin-left:1.5cm;}
    
    .font14{font-size:14pt;}
    .font16{font-size:16pt;}
    .font18{font-size:18pt;}
    
    .left{text-align:left;}
    .center{text-align:center;}
    .right{text-align:right;}
    
    table tr td{
        border:1px solid;
    }
    th{
        border:1px solid;
    }
    
</style>   

<div class="row font16" id="mainpage">

        <div class="container">
            <div class="font18 center" >                
                 <img src="Login_v1/images/moph-logo.png" alt="moph logo" width="150"/>
            </div>
        </div>

        <div class="container font16" style="margin-top:8pt;">
            <div style="margin-left:102mm" >
                เขียนที่ สำนักงานปลัดกระทรวงสาธารณสุข<br>
            </div>
            <div style="margin-left:102mm" >
                <?php echo formaldate(date('Y-m-d')) ?>
            </div>
        </div>

        <div class="container" style="margin-top:8pt;">
            <div>
                เรื่อง&nbsp;&nbsp;&nbsp;&nbsp;ข้อมูลผลการสอบ<br>
                เรียน&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $name ?>
            </div>
        </div>    

        <div class="container" style="margin-top:10pt;">
            <div>
                <?php
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo "ตามที่ท่านแจ้งความประสงค์ขอทราบคะแนนสอบเพื่อวัดความรู้ความสามารถที่ใช้เฉพาะ<br>";           
                    echo "ตำแหน่ง (ภาค ข.) ในการสอบแข่งขันพื่อบรรจุบุคคลเข้ารับราชการ  ใน".$result['round_title'];           
                    echo " เมื่อ".formaldate($result['round_exam_date'])." นั้น";
                ?>
            </div>
        </div>
    
        <div class="container" style="margin-top:5pt;">
            <div>    
                <?php
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo "กลุ่มงานสรรหาบุคคล กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข จึงขอ<br>แจ้งผลการสอบ ดังนี้<br>";
                ?>
            </div>
        </div>
    
        <div class="container" style="margin-top:12pt;">
            <div>    
                <?php
                    echo "<b>ชื่อ - สกุล</b> : " . $name . "<br>";
                    echo "<b>เลขประจำตัวสอบ</b> : " . $result['detail_id'] . "<br>";
                    echo "<b>ตำแหน่ง</b> : " . $result['detail_position'] . "<br>";
                ?>
            </div>
        </div>

        <div class="font16 container" style="margin-top:15px;"> 
            <table cellspacing="0" style="width:100%; font-size:22px;" autosize="1">
                <tr>
                    <th style="width:170px;">การสอบ</th>
                    <th style="width:80px;">คะแนนเต็ม</th>
                    <th style="width:90px;">คะแนนที่ได้</th>
                    <th style="width:110px;">ผลการสอบ</th>
                    <th>ผู้ตรวจสอบ</th>
                </tr>                    
                <tr>
                    <td style="text-align:center; vertical-align:middle; height:110px;">ความรู้ความสามารถที่ใช้<br>เฉพาะตำแหน่ง (ภาค ข.)</td>
                    <td style="text-align:center; vertical-align:middle;"><?php echo $result['round_max_score'] . '<br>คะแนน'; ?></td>
                    <td style="text-align:center; vertical-align:middle;"><?php echo $result['detail_score'] . '<br>คะแนน'; ?></td>
                    <td style="text-align:center; vertical-align:middle;"><?php echo $resultshow ?></td>
                    <td style="text-align:center; vertical-align:middle;"></td>
                </tr>                    
            </table>            
        </div>
    
        <div class="container" style="text-align:right; margin-top:40px;">
            <div>    
                <?php
                    echo "กลุ่มงานสรรหาบุคคล กองบริหารทรัพยากรบุคคล<br>";
                    echo "สำนักงานปลัดกระทรวงสาธารณสุข";
                ?>
            </div>
        </div>

</div>


<?php
$html = ob_get_contents();
ob_end_clean();


//$mpdf = new mPDF();
//$mpdf = new mPDF();
$mpdf = new mPDF('th', 'A4-L', '0', '');
$mpdf->SetAutoFont();
$mpdf->SetDisplayMode('fullpage');
$mpdf->WriteHTML($html, 2);
$mpdf->Output();
?>