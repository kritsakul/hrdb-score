<?PHP

header("Content-type:application/json; charset=UTF-8");
require_once("connections/conn.php");

//รับค่าข้อมูล

$tmpUsername =  trim($_POST["val-username"]);
$tmpPassword =  trim($_POST["val-password"]);
$md5 = md5($tmpPassword);
  
    //login ได้เฉพาะ กลุ่มงาน สรรหา->งานสอบแข่งขัน เท่านั้น
    //และ กลุ่มงาน pms&it->admin เท่านั้น
    //work_id=32 คือ งาน it
    //work_id=10 คือ งาน สอบแข่งขัน
    //work_id=38 คือ หัวหน้ากลุ่มงาน สรรหา)
    $sql = "
        SELECT 
            * 
        FROM 
            user 
        WHERE 
            username=:username AND 
            password=:md5 AND
            status=1 AND
            (
                (permission=1 AND work_id=32) ||
                (work_id=10) ||
                (work_id=38)
            )
    ";
    $stmt = $conn->prepare($sql);
    $stmt->bindParam(':username', $tmpUsername);
    $stmt->bindParam(':md5', $md5);
    $stmt->execute();
    $dataStmt = $stmt->fetch(PDO::FETCH_ASSOC);
    $tmpCount = $stmt->rowCount();
    $tmpName = $dataStmt['name'];    
    
    if($tmpCount == 1){ //มีข้อมูล
        setcookie("cookUsername", $tmpUsername, time() + (7 * 24 * 60 * 60)); // 1 อาทิตย์
        setcookie("cookName", $tmpName, time() + (7 * 24 * 60 * 60)); // 1 อาทิตย์
        $arr = array(
            "status"=>'1',
            "msg"=>'เสร็จสมบูรณ์'
        );
        
    }else{ //ไม่มีข้อมูล
        $arr = array(
            "status"=>'0',
            "msg"=>'ไม่มีชื่อผู้ใช้นี้ หรือรหัสผ่านผิดพลาด กรุณาตรวจสอบ'
        );
        
    }        

echo json_encode($arr);
    
?>