<div class="sidebar" data-color="green" data-image="assets/img/sidebar-9.jpg">
    <!--            
        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag            
    -->
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                <b>ระบบดูผลคะแนนสอบออนไลน์</b>
            </a>
        </div>

        <ul class="nav">
                        

            <?php
            if($tmpMainUsername !== ""){
            ?>            
         
                <li>
                    <a href="manage.php">
                        <i class="pe-7s-note2"></i>
                        <p>จัดการประกาศ</p>
                    </a>
                </li>  
<!--                
                <li>
                    <a href="">
                        <i class="pe-7s-notebook"></i>
                        <p>คู่มือการใช้งาน</p>
                    </a>
                </li>            -->
                
            <?php
            }
            ?>            
                       
            
            <?php
            if($tmpMainUsername == ""){
            ?>            
                <li class="active-pro">
                    <a href="login.php">
                        <i class="pe-7s-user"></i>
                        <p>ผู้ดูแลระบบ</p>
                    </a>
                </li>
            <?php
            }else{
            ?>
                <li class="active-pro" style="background-color:#F00 !important;" >
                    <a href="#" onclick="funcLogout();" style="background-color:#F00 !important;">
                        <i class="pe-7s-power"></i>
                        <p>ออกจากระบบ</p>
                    </a>
                </li>            
            <?php
            }
            ?>
                
                
        </ul>
    </div>
</div>


<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li style="background:green;">
                        <a href="">
                            <p style="color:#FFF;">ยินดีต้อนรับ <?php echo $tmpMainName ?></p>
                        </a>
                    </li>                                
                    <li class="separator hidden-lg"></li>
                </ul>
            </div>
            
        </div>
    </nav>