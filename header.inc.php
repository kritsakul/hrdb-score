    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="style.css" rel="stylesheet" type="text/css"/>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>

    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    
    <link href="vendors/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />    

    <!-- data table css -->
    <link rel="stylesheet" href="vendors/datatable/datatables.css">
    <link rel="stylesheet" href="vendors/datatable/DataTables-1.10.16/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="vendors/datatable/Buttons-1.5.1/css/buttons.dataTables.css">    
    
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <style>
        body, p, div, span, button, a, td, label, h1, h2, h3, h4, h5, h6{
            font-family: 'Prompt', sans-serif !important;
        }
    </style>