 <?php  
 
        require_once("connections/conn.php");
        $tmpMainUsername = trim(filter_input(INPUT_COOKIE, 'cookUsername'));
        $tmpMainName = trim(filter_input(INPUT_COOKIE, 'cookName'));                 
        if ($tmpMainUsername == ""){
            header("Location: index.php");
            exit();        
            
        }else{            
             //เช็คว่า ถึงจะมี username วิ่งเข้ามาก็ให้เช็คว่า มีข้อมูลอยู่ใน ฐานข้อมูลจริงหรือไม่   
            //และต้องเป็น username ของ "กลุ่มงานสรรหา เท่านั้น"
            $sql = "SELECT 
                    user_id,
                    permission
                FROM 
                    user
                WHERE 
                    username = :tmpUsername AND
                    (
                        (permission=1 AND work_id=32) ||
                        (work_id=10) ||
                        (work_id=38)
                    )
            ";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':tmpUsername', $tmpMainUsername);
            $stmt->execute();
            $tmpUserCount = $stmt->rowCount();      
            if($tmpUserCount <= 0){ //ไม่มีข้อมูล จะ redirect ไปหน้าหลัก
                header("Location: index.php");
                exit();                
            }else{//ถ้ามีข้อมูล                
                $tmpDataMainUser = $stmt->fetch(PDO::FETCH_ASSOC);
                //permission 1=admin, 2=user
                $tmpMainUserID = $tmpDataMainUser['user_id'];
                $tmpMainPermission = $tmpDataMainUser['permission'];
            }
            
        }

  ?>