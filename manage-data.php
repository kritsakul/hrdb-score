<?php
    
require_once 'redirect.inc.php';
require_once 'function.inc.php';

$tmpUsername = $tmpMainUsername;
$sql = "
    SELECT 
        *
    FROM             
        score_round
    ORDER BY 
        round_exam_date DESC
";    
$query = $conn->prepare($sql);
$query->execute();

//ถ้าระบบยังไม่มีข้อมูล
$i = 0;    
$tmpJson = array();

//$result = $query->fetchAll(PDO::FETCH_ASSOC);
//print_r($result);
//ค้นหาข้อมูลจำนวนใน score_detail ว่ามีกี่คน

//นับจำนวนผู้เข้าสอบ
$sql = "
    SELECT
        Count(score_detail.detail_no) as total1,
        round_no
    FROM
        score_detail
    GROUP BY
        round_no
";
$stmt = $conn->prepare($sql);
$stmt->execute();
$fetch_score = $stmt->fetchAll(PDO::FETCH_ASSOC);


while ($result = $query->fetch(PDO::FETCH_ASSOC)) {        
              
        $roundno = $result['round_no'];
        $title = $result['round_title'];
        $examdate = $result['round_exam_date'];
        $maxscore = $result['round_max_score'];
        $publishdate = $result['round_publish_date'];
        $unpublishdate = $result['round_unpublish_date'];
        if($publishdate == ""){
            $fulldate = "ยังไม่ระบุ";
            $datediff = "-";
        }else{
            $datediff = dateDifference(date('Y-m-d'), $unpublishdate);
            if($datediff >= 90){
                $datediff = 90;
            }elseif($datediff <= 89 && $datediff > 0){
                $datediff++;
            }else{
                $datediff = 0;
            }
            $fulldate = dateintertothai($publishdate, '');
        }
                
        if($result['round_status'] == 0){
            $statusshow = '<span style="color:red">ยังไม่ประกาศ</span>';
            
        }elseif($result['round_status'] == 1){            
            //ุถ้าวันที่ปัจจุบัน มากกว่า วันที่ unpublishdate
            $statusshow = '<span style="color:green">เปิดประกาศผล</span>';
            
        }    
        
        $btnmanage = '<a class="btn btn-primary btn-xs btn-fill btn-block" href="manage-detail.php?roundno='.$roundno.'">จัดการ</a>';
        
        $total = 0;
        foreach($fetch_score as $detail){
            if($detail['round_no'] == $roundno){
                $total = $detail['total1'];
                break;
            }
        }
        
        if($datediff <= 0){
            $datediffshow = '<span class="text-red">'.$datediff.'</span>';
        }elseif($datediff <= 20){
            $datediffshow = '<span class="text-orange">'.$datediff.'</span>';
        }else{
            $datediffshow = '<span class="text-green">'.$datediff.'</span>';
        }
        
        $tmpJson[$i]['no'] = $i+1;
        $tmpJson[$i]['roundno'] = $roundno;
        $tmpJson[$i]['title'] = $title; 
        $tmpJson[$i]['examdate'] = dateintertothai($examdate);   
        $tmpJson[$i]['publishdate'] = $fulldate;
        $tmpJson[$i]['datediff'] = '<span class="text-red">'.$datediffshow.'</span>';
        $tmpJson[$i]['total'] = $total;   
        $tmpJson[$i]['status'] =$statusshow;  
        $tmpJson[$i]['manage'] =$btnmanage;  
        
        $i++;                                         
}

//print_r($tmpJson);
echo json_encode($tmpJson);


?>