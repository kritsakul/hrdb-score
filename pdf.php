<?php
//header("Content-type:text-plain; charset=UTF-8");
error_reporting(E_ALL); 
ini_set('display_errors', 1);

require_once("connections/conn.php");
require_once("function.inc.php");
require_once('vendors/tcpdf-master/tcpdf.php');

$roundno = trim($_GET["roundno"]);
$detailno = trim($_GET["detailno"]);
$token = trim($_GET["token"]);
$date=date('Y-m-d');

$sql = "
    SELECT 
        *
    FROM             
        score_round, score_detail
    WHERE
        score_round.round_no=score_detail.round_no AND
        score_round.round_no=:roundno AND
        score_detail.detail_no=:detailno AND
        score_detail.detail_token=:token AND
        score_round.round_unpublish_date>=:date
    LIMIT
        0,1
";    
$stmt = $conn->prepare($sql);
$stmt->bindParam(':roundno',$roundno);
$stmt->bindParam(':detailno',$detailno);
$stmt->bindParam(':token',$token);
$stmt->bindParam(':date',$date);
$stmt->execute();

if($stmt->rowCount() == 0){
    echo "ไมมีข้อมูล กรุณาตรวจสอบ";
    exit();
    
}else{
    $result = $stmt->fetch(PDO::FETCH_ASSOC);    
    $name = $result['detail_prename'] . $result['detail_fname'] . ' ' . $result['detail_lname'];
    
    $resultstatus = trim($result['detail_result']);
    if($resultstatus == 'ขส'){
        $resultshow = 'ขาดสอบ';
    }elseif($resultstatus == 'ตก'){
        $resultshow = 'ไม่ผ่าน';
    }elseif($resultstatus == 'ได้'){
        $resultshow = 'ผ่าน';
    }
    
}

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

$top = 20;
$left = 28;
$right = 15;

$pdf->SetMargins($left, $top, $right);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set font
//$fontname = $pdf->addTTFfont('font\thsarabun\thsarabunnew-webfont.ttf', 'TrueTypeUnicode', '' , 32);
$pdf->SetFont('thsarabunnew', '', 16);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '
    <div style="text-align:center">           
        <img src="Login_v1/images/moph-logo.png" alt="moph logo" width="100"/>
    </div>
';
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


$html = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$html .= 'เขียนที่ สำนักงานปลัดกระทรวงสาธารณสุข<br>';
$html .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
$html .= formaldate(date('Y-m-d')) . "<br>";
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

$html = "เรื่อง&nbsp;&nbsp;&nbsp;&nbsp;ข้อมูลผลการสอบ<br>";
$html .= "เรียน&nbsp;&nbsp;&nbsp;".$name.'<br>';
$pdf->writeHTML($html, true, false, true, false, '');


$html = '

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
ตามที่ท่านแจ้งความประสงค์ขอทราบคะแนนสอบเพื่อวัดความรู้ความสามารถที่ใช้เฉพาะตำแหน่ง<br>
(ภาค ข.) ในการสอบแข่งขันเพื่อบรรจุบุคคลเข้ารับราชการ  ใน'.$result['round_title'].'   
เมื่อ '.formaldate($result['round_exam_date']).' นั้น  
';
$pdf->writeHTML($html, true, false, true, false, '');

$html = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 กลุ่มงานสรรหาบุคคล กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข จึงขอ<br>แจ้งผลการสอบ ดังนี้<br>';
$pdf->writeHTML($html, true, false, true, false, '');

$html = '<b>ชื่อ - สกุล</b> : '.$name.'<br>
<b>เลขประจำตัวสอบ</b> : '.$result['detail_id'].'<br>
<b>ตำแหน่ง</b> : '.$result['detail_position'].'<br>';
$pdf->writeHTML($html, true, false, true, false, '');

$html = '<table cellspacing="0" style="width:auto; " border="1" autosize="1">
<tr>
    <th style="width:197px; text-align:center;">การสอบ</th>
    <th style="width:80px; text-align:center;">คะแนนเต็ม</th>
    <th style="width:80px; text-align:center;">คะแนนที่ได้</th>
    <th style="width:110px; text-align:center;">ผลการสอบ</th>
</tr>                    
<tr>
    <td style="text-align:center; height:90px;"><div></div>ความรู้ความสามารถที่ใช้<br>เฉพาะตำแหน่ง (ภาค ข.)</td>
    <td style="text-align:center;"><div></div>'.$result['round_max_score'].'<br>คะแนน</td>
    <td style="text-align:center;"><div></div>'.$result['detail_score'].'<br>คะแนน</td>
    <td style="text-align:center;"><div></div>'.$resultshow.'</td>
</tr>                    
</table>';
$pdf->writeHTML($html, true, false, true, false, '');


$html = '<span style="text-align:right;">กลุ่มงานสรรหาบุคคล กองบริหารทรัพยากรบุคคล<br>
สำนักงานปลัดกระทรวงสาธารณสุข</span>';
$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();
// ---------------------------------------------------------

ob_end_clean();

//Close and output PDF document
$filename = $result['detail_id'] . "-" . date("d-m-Y H:i:s");
$pdf->Output($filename, 'I');

//============================================================+
// END OF FILE
//============================================================+