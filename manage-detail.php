<!doctype html>
<html lang="en">
    <head>

        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <title>รายละเอียดประกาศ - กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</title>

        <?php

            require_once 'redirect.inc.php';
            require_once 'cookie.inc.php';
            require_once 'function.inc.php';
            require_once 'header.inc.php';

            //เช็คข้อมูล ประกาศนี้มีข้อมูลหรือไม่
            $roundno = filter_input(INPUT_GET, 'roundno');
            $sql = "SELECT * FROM score_round WHERE round_no=:roundno LIMIT 0,1";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':roundno', $roundno);
            $stmt->execute();

            //ถ้าไม่มีข้อมูล actid จะไม่ redirect ไปหน้าหลัก
            if ($stmt->rowCount() <= 0) {
                header("Location: index.php"); /* Redirect browser */
                exit();
            } else {
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
            } 

            $roundno = $result['round_no'];
            $title = $result['round_title'];
            $examdate = $result['round_exam_date'];
            $maxscore = $result['round_max_score'];
            $publishdate = trim($result['round_publish_date']);
            $unpublishdate = trim($result['round_unpublish_date']);
            if($publishdate == ""){
                $fulldate = "ยังไม่ระบุ";
            }else{                
                $datediff = intval(dateDifference(date('Y-m-d'), $unpublishdate));
                if($datediff >= 90){
                    $datediff = 90;
                }elseif($datediff <= 89 && $datediff > 0){
                    $datediff++;
                }else{
                    $datediff = 0;
                }                
                
                $fulldate = dateintertothai($publishdate) . " <b>ถึง</b> " . dateintertothai($unpublishdate) . " <span style='color:red;'>(เหลือเวลา ".$datediff." วัน)</span>";
            }

            $status = $result['round_status'];
            if($result['round_status'] == 0){
                $statusshow = '<span style="color:red">ยังไม่ประกาศ</span>';

            }elseif($result['round_status'] == 1){   
                $statusshow = '<span style="color:green">เปิดประกาศผล</span>';
            }                
            
       
            //ตก ขส ได้
            $sql = "SELECT detail_result, detail_login_date FROM score_detail WHERE round_no=:roundno";
            $stmt_check = $conn->prepare($sql);
            $stmt_check->bindParam(':roundno',$roundno);
            $stmt_check->execute();
            $allresult = $stmt_check->rowCount(); //จำนวนข้อมูลทั้งหมด        
            $fetchall = $stmt_check->fetchAll(PDO::FETCH_ASSOC);
            $arrPass = array();
            $arrView = array();
            $arrPass['fail'] = 0;
            $arrPass['pass'] = 0;
            $arrPass['no'] = 0;
            $arrView['view'] = 0;
            $arrView['no'] = 0;           

            foreach($fetchall AS $result_check){
                if(trim($result_check['detail_result']) == 'ตก'){
                    $arrPass['fail']++;
                }elseif(trim($result_check['detail_result']) == 'ได้'){
                    $arrPass['pass']++;
                }elseif(trim($result_check['detail_result']) == 'ขส'){
                    $arrPass['no']++;
                }
                
                if(trim($result_check['detail_login_date']) !== "" || trim($result_check['detail_login_date']) != null){
                    $arrView['view']++;
                }else{
                    $arrView['no']++;
                }
            }
            
            if($allresult > 0){
                $percentfail = number_format(($arrPass['fail'] * 100)/$allresult,2);
                $percentpass = number_format(($arrPass['pass'] * 100)/$allresult,2);
                $percentno = number_format(($arrPass['no'] * 100)/$allresult,2);
                $percentview = number_format(($arrView['view'] * 100)/$allresult,2);
                $percentnotview = 100 - $percentview;
                
            }else{
                $percentfail = 0;
                $percentpass = 0;
                $percentno = 0;
                $percentview = 0;
                $percentnotview = 0;
                
            }
        ?>     

        <style>
            .header{
                padding:20px !important;
            }
            th{
                background-color:#66cc00 !important;
                color:#FFF !important;
                font-size:16px;
            }
            .td-middle{
                vertical-align:middle;                
            }
            td{
                padding:5px !important;
            }
            .text-center{
                text-align:center;
            }
            .vr-middle{
                vertical-align:middle;
            }
            .vr-top{
                vertical-align:top !important;
            }
             .text-red{
                 color:red;
             }            
            .trhover{
                background-color: #ffff99 !important;
                cursor:pointer !important;
            }
            .ct-label{
                font-weight:bold !important;
                color:white !important;
                fill:black !important;
                font-size:16px;
            }
        </style>

    </head>
    <body>

        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">

                        <div class="row">                            
                            <div class="col-md-12">

                                <div class="card"> 

                                        <p style="font-size:24px; padding:20px 10px 0px 20px;">
                                            <i class="pe-7s-note2"></i> <u><a href="manage.php" class="text-green"> จัดการข้อมูลประกาศผลคะแนน</a></u> > จัดการข้อมูล
                                        </p>                                    

                                        <div class="content" >                                        
                                                <div class="row">               
                                                    <div class="col-xs-12">

                                                        <div class="box" style="margin-bottom:0px !important;">
                                                            <!-- /.box-header -->
                                                            <div class="box-body table-responsive">      

                                                                    <table class="table table-responsive font16 table-bordered">
                                                                        <tr>
                                                                            <td width='25%' class="vertical-top"><b>ตำแหน่งที่สอบ</b></td>                                          
                                                                            <td class="vertical-top"><b><?php echo $title ?></b> <a target="_blank" class="text-red" href="./?roundno=<?php echo $roundno ?>"><u>(<i class="fa fa-external-link" aria-hidden="true"></i> ดูตัวอย่างหน้าเว็บ)</u></a></td>
                                                                        </tr>     
                                                                         <tr>
                                                                            <td><b>วันที่สอบ</b></td>            
                                                                            <td><?php echo dateintertothai($examdate) ?></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>คะแนนเต็ม</b></td>                                          
                                                                            <td><?php echo $maxscore ?> คะแนน</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td><b>วันที่ เปิด-ปิด ประกาศผลสอบ</b></td>                                          
                                                                            <td>
                                                                                <?php echo $fulldate?>                                                           
                                                                            </td>
                                                                        </tr>             

                                                                        <tr>
                                                                            <td><b>สถานะ</b></td>                                          
                                                                            <td>
                                                                                <?php echo $statusshow ?> 
                                                                                
                                                                                <button  data-toggle='modal' data-target='#myModalEdit'
                                                                                    data-title="<?php echo $title ?>" 
                                                                                    data-examdate='<?php echo dateintertothai($examdate) ?>' 
                                                                                    data-maxscore='<?php echo $maxscore ?>'
                                                                                    data-status='<?php echo $status ?>'
                                                                                    <?php
                                                                                         if(trim($result['round_publish_date']) !== ""){
                                                                                            $publishdateshow = dateintertothai($result['round_publish_date']);                                                                                         
                                                                                         }else{
                                                                                            $publishdateshow = dateintertothai(date('Y-m-d'));      
                                                                                         }                                                                                     
                                                                                    ?>
                                                                                    data-publishdate='<?php echo $publishdateshow;  ?>'
                                                                                    class="btn btn-fill btn-xs btn-warning"><i class="fa fa-pencil-square-o" ></i> แก้ไขประกาศ/สถานะ</button>   
                                                                                <button class="btn btn-xs btn-fill btn-danger" data-target='#myModalDelete'  data-toggle='modal'><i class="fa fa-minus-circle"></i> ลบประกาศ</button>   
                                                                                
                                                                            </td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td><b>QR-Code กับ Short link</b></td>                                          
                                                                            <td>
                                                                                <a style="color:green;" href="manage-qrcode.php?roundno=<?= $roundno ?>"><u><i class="fa fa-qrcode" ></i>คลิกเพื่อดู</u></a> 
                                                                            </td>
                                                                        </tr>

                                                                    </table>

                                                            </div>   

                                                        </div>
                                                        <!-- /.box -->    

                                                    </div>
                                                    <!-- /.col -->       

                                            </div>  
                                    </div>
                                        
                                </div>

                                <div class="card" style="padding-bottom:30px;">
                                    <div class="content" >                                        
                                        <div class="row">               
                                            <div class="col-xs-12">

                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-2 float-right" style="margin-bottom:5px;"  data-target='#myModalDeleteScore'  data-toggle='modal' data-actid="<?php echo $tmpActID ?>" >
                                                            <div class="btn btn-fill btn-sm btn-danger btn-block"><i class="fa fa-minus-circle"></i> ลบผลสอบ</div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-3 float-right" style="margin-bottom:5px;">
                                                            <a href="manage-score.php?roundno=<?= $roundno ?>" class="btn btn-fill btn-sm btn-primary btn-block"><i class="fa fa-plus" ></i> นำเข้าข้อมูลผลคะแนน</a>
                                                        </div>
                                                    </div>

                                                    <div class="clearfix"></div>                      

                                                    <div class="container-fluid" style="margin-top:10px;">
                                                        <div class="row">                                        
                                                            <div class="table-responsive" style="width:auto;" >                                                    
                                                                <form autocomplete="false">
                                                                    <table id="tbMain" class="table table-striped table-bordered highlight2"  cellspacing="0" width="100%">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center" width="5%">ลำดับ</th>
                                                                                <th class="text-center" width="10%">เลขประจำตัวสอบ</th>
                                                                                <th class="text-center" width="25%">ตำแหน่ง</th>
                                                                                <th class="text-center" width="25%">ชื่อ - นามสกุล</th>
                                                                                <th class="text-center" width="10%">รหัสประชาชน</th>
                                                                                <th class="text-center" width="5%">GPA</th>
                                                                                <th class="text-center" width="5%">คะแนนที่ได้</th>  
                                                                                <th class="text-center" width="5%">เกณฑ์การตัดสิน</th>
                                                                                <th class="text-center" width="10%">เข้ามาดูล่าสุด</th>
                                                                                <th class="text-center" width="10%">จำนวนครั้งการดู</th>
                                                                            </tr>                                                
                                                                        </thead>
                                                                        <tbody>                                                                    
                                                                        </tbody>
                                                                    </table>   
                                                                </form>
                                                            </div>                                         
                                                        </div>
                                                    </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-bottom:1px !important;">

                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="header" style="padding-bottom:5px !important; margin-bottom:0px !important;">
                                                <h4 class="title">สถิติการสอบ</h4>
                                            </div>
                                            <div class="content" style="margin-top:0px; padding-top:0px;">
                                                <div id="chartPass" class="ct-chart ct-perfect-fourth" style="margin-top:0px; margin-bottom:0px;"></div>
                                                <div class="footer">
                                                    <div class="legend">
                                                        <i class="fa fa-circle text-info"></i>ผ่าน(<?php echo $arrPass['pass'] ?>)
                                                        <i class="fa fa-circle text-danger"></i>ไม่ผ่าน(<?php echo $arrPass['fail'] ?>)
                                                        <i class="fa fa-circle text-warning"></i>ขส.(<?php echo $arrPass['no'] ?>) => <b>รวม <?php echo $allresult ?> คน</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>

                                    <div class="col-md-6">
                                        <div class="card">
                                            <div class="header" style="padding-bottom:5px !important; margin-bottom:0px !important;">
                                                <h4 class="title">สถิติผู้เข้าดูคะแนน</h4>
                                            </div>
                                            <div class="content" style="margin-top:0px; padding-top:0px;">
                                                <div id="chartView" class="ct-chart ct-perfect-fourth" style="margin-top:0px; margin-bottom:0px;"></div>
                                                <div class="footer">
                                                    <div class="legend">
                                                        <i class="fa fa-circle text-info"></i>ดูแล้ว(<?php echo $arrView['view']?>)
                                                        <i class="fa fa-circle text-danger"></i>ยังไม่ดู(<?php echo $arrView['no']?>)
                                                        => <b>รวม <?php echo $allresult ?> คน</b>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                        
                                    </div>                                 

                                </div>
                                
                                
                            </div>
                        </div>

                    </div>
                </div>

                <?php
                    require_once 'footer.inc.php';                    
                ?>

            </div>
        </div>

        <!------------------------------------------------------------------------------------------>
        <!------------------------------------ myModal Edit  ------------------------------------>
        <!------------------------------------------------------------------------------------------>
        <div class="modal fade" id="myModalEdit" role="dialog">            
            <div class="modal-dialog modal-warning modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" style="background-color:orange; color:#FFF;">
                        <h4 class="modal-title" >แก้ไขข้อมูล</h4>
                    </div>

                    <div class="modal-body font16">

                        <div class="form-horizontal form-label-left" style="padding-bottom:2px; ">

                            <form id="mainFormEdit" class="form-horizontal">

                                <input type="hidden" id="txtRoundNo" name="txtRoundNo" value="<?php echo $roundno ?>">
                                <input type="hidden" id="txtUsername" name="txtUsername" value="<?php echo $tmpMainUsername ?>">

                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label  text-red">*ตำแหน่งที่สอบ</label>
                                    <div class="col-sm-8">
                                        <textarea type="text" maxlength="900"  class="form-control font16 required" rows="4" required  id="txtTitle" name="txtTitle" placeholder="ใส่ชื่อตำแหน่งที่สอบ เช่น ตำแหน่งนักจัดการงานทั่วไปปฏิบัติการ นักวิชาการเงินและบัญชีปฏิบัติการ นักวิชาการพัสดุปฏิบัติการ " autocomplete="off" value="" ></textarea>
                                    </div> 
                                </div>                

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label  text-red">*วันที่สอบ</label>
                                    <div class="col-sm-8">                                    
                                        <input type="text" class="form-control font16  pull-right selector" id="txtExamDate" name="txtExamDate" required autocomplete="off"  placeholder="วันที่ทำการสอบ" value=""> 
                                    </div>                           
                                </div>                                 
                                
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label  text-red">*คะแนนเต็ม</label>
                                    <div class="col-sm-8">
                                        <input type="text" maxlength="5"  class="form-control font16 num required" required  id="txtMaxScore" name="txtMaxScore" placeholder="ใส่คะแนนสอบ" autocomplete="off" value="200" ></input>
                                    </div> 
                                </div>

                                <hr>
                                <br>
                                
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">*วันที่ประกาศผลสอบ</label>
                                    <div class="col-sm-8">                                    
                                        <input type="text" class="form-control font16  pull-right selector" id="txtPublishDate" name="txtPublishDate" autocomplete="off"  placeholder="วันที่ประกาศผลสอบ" value=""> 
                                    </div>                           
                                </div>    

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">*สถานะ</label>
                                    <div class="col-sm-8">                                    
                                        <select class="form-control" id="selStatus" name="selStatus">
                                            <option value="0">ยังไม่ประกาศ</option>
                                            <option value="1">เปิดประกาศผล</option>
                                        </select>
                                    </div>                           
                                </div>  

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-fill btn-default" data-dismiss="modal">ปิด</button>
                                    <button type="submit" class="btn btn-fill btn-success" id="btnSaveEdit">บันทึกการแก้ไขข้อมูล</button>
                                </div>                                
                            </form>

                        </div>  
                    </div>

                </div>
            </div>
        </div>         

        <!----------------------------------------------------------------------------------------->
        <!--------------------------------------- ลบประกาศ  ----------------------------------->
        <!----------------------------------------------------------------------------------------->
        <div class="modal fade" id="myModalDelete" role="dialog">            
            <div class="modal-dialog modal-danger">
                <form id="mainFormDel">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header modal-danger" style="background-color:#cc0000; color:#FFF;">
                            <h4 class="modal-title" >คำเตือน</h4>
                        </div>                    
                        <div class="modal-body">
                            <input type="hidden" id="txtRoundNoDel" name="txtRoundNoDel"  readonly autocomplete="off" value='<?php echo $roundno ?>'>
                            <div style="font-size:16px;">คุณต้องการลบข้อมูล ประกาศนี้ใช่หรือไม่ กรุณายืนยัน <br>ทำการตรวจสอบให้ดีอีกครั้ง <br><span class="text-red"><u><b>เพราะลบแล้วจะเรียกคืนข้อมูลเดิมไม่ได้ กรุณายืนยัน !!!</b></u></span></div>
                            <br>
                            ใส่รหัสผ่านของผู้ใช้งานปัจจุบัน เพื่อยืนยันดำเนินการลบ
                            <input type="password" id="txtConfirmPass" name="txtConfirmPass" class="form-control" required>
                        </div>
                        <div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">ยกเลิก</button>
                            <button type="submit"  data-toggle="modal"   class="btn btn-danger btn-fill">ยืนยันการลบ</button>
                        </div>
                        <input style="display:none" type="text" name="fakeusernameremembered"/>
                    </div>
                </form>
            </div>
        </div>        


        <!------------------------------------------------------------------------------------------->
        <!--------------------------------------- ลบผลคะแนน ----------------------------------->
        <!------------------------------------------------------------------------------------------->
        <div class="modal fade" id="myModalDeleteScore" role="dialog">            
            <div class="modal-dialog modal-danger">
                <form id="mainFormDelScore">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header modal-danger" style="background-color:#cc0000; color:#FFF;">
                            <h4 class="modal-title" >คำเตือน</h4>
                        </div>                    
                        <div class="modal-body">
                            <input type="hidden" id="txtRoundNoDelScore" name="txtRoundNoDelScore"  readonly autocomplete="off" value='<?php echo $roundno ?>'>
                            <div style="font-size:16px;">คุณต้องการลบข้อมูล <u>"ผลคะแนนทั้งหมดนี้ใช่หรือไม่"</u> กรุณายืนยัน <br>ทำการตรวจสอบให้ดีอีกครั้ง <br><span class="text-red"><u><b>เพราะลบแล้วจะเรียกคืนข้อมูลเดิมไม่ได้ กรุณายืนยัน !!!</b></u></span></div>
                            <br>
                            ใส่รหัสผ่านของผู้ใช้งานปัจจุบัน เพื่อยืนยันดำเนินการลบ
                            <input type="password" id="txtConfirmPassDelScore" name="txtConfirmPassDelScore" class="form-control" required>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-fill" data-dismiss="modal">ยกเลิก</button>
                            <button type="submit"  data-toggle="modal"   class="btn btn-danger btn-fill">ยืนยันการลบ</button>
                        </div>
                        <input style="display:none" type="text" name="fakeusernameremembered"/>
                    </div>
                </form>
            </div>
        </div>           


    </body>

        <!--   Core JS Files   -->
        <!-- jQuery 2.2.3 -->
        <!--<script src="vendors/jquery/jquery-2.2.3.min.js"></script>-->
        <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>

        <!-- Bootstrap 3.3.7 -->
        <script src="vendors/bootstrap/js/bootstrap.js" type="text/javascript"></script>

        <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
        <script src="assets/js/light-bootstrap-dashboard.js"></script>

        <!-- jquery validation -->
        <script src="vendors/jquery-validation/dist/jquery.validate.js"  type="text/javascript"></script>     
        <script src="vendors/jquery-validation/dist/localization/messages_th.js"  type="text/javascript"></script> 

        <!-- datepicker -->  
        <script src="vendors/jQueryCalendarThai_Ui1.11.4/jquery-ui-1.11.4.custom.js" type="text/javascript"></script>        

        <!-- datatables -->  
        <script src="vendors/datatable/DataTables-1.10.16/js/jquery.dataTables.js"></script>
        <!--<script src="vendors/datatable/Buttons-1.5.1/js/dataTables.buttons.js"></script>
        <script src="vendors/datatable/Buttons-1.5.1/js/buttons.print.js"></script>    
        <script src="vendors/datatable/Buttons-1.5.1/js/buttons.html5.js"></script>
        <script src="vendors/datatable/JSZip-2.5.0/jszip.js"></script>
        <script src="vendors/datatable/pdfmake.min.js"></script>         
        <script src="vendors/datatable/vfs_fonts.js"></script>               -->

        <!--  Charts Plugin -->
        <script src="assets/js/chartist.min.js"></script>        
        <script src="assets/js/demo.js"></script>


        <script type="text/javascript">

               //demo.initChartist();
                var roundno = $("#txtRoundNo").val();
                $('#tbMain').DataTable({
                    "dom": 'ฺ<"top"f>Brt<"top"ilp>',
                     "responsive": true,
                     "lengthMenu": [[15,  50, 100, -1], [15, 50, 100, "ทั้งหมด"]],
                     "pageLength": 15,
                     "processing": true,
                    "columns": [					
                            { "data": "no"},
                            { "data": "detail_id"},
                            { "data": "position"},
                            { "data": "name"},
                            { "data": "customerid"},
                            { "data": "gpa"},
                            { "data": "score"},
                            { "data": "result"},
                            { "data": "logindate"},
                            { "data": "view"}
                    ],
                    "ajax": {              
                        "type" : "POST",
                        "data" : {roundno: roundno},
                        "url" : "manage-data-detail.php",
                        "dataSrc": ""
                    },
                   "language": {
                         "sProcessing":   "กำลังดำเนินการ...",
                         "sLengthMenu":   "แสดง _MENU_ แถว",
                         "sZeroRecords":  "ไม่พบข้อมูล",
                         "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                         "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
                         "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
                         "sInfoPostFix":  "",
                         "sSearch":       "ค้นหา: ",
                         "sUrl":          "",
                         "oPaginate": {
                             "sFirst":    "หน้าแรก",
                             "sPrevious": "ก่อนหน้า",
                             "sNext":     "ถัดไป",
                             "sLast":     "หน้าสุดท้าย"
                         }
                     },
                      "columnDefs": [
                            {
                                "targets": [0],
                                "visible": false,
                                "searchable": false
                            },
                            {
                                targets: [4,5,6,7,8,9],
                                className: 'dt-body-center'
                            }
                       ]
                })

                // --------------------- ทำการลบ ข้อมูลประกาศ -----------------------
                $( "#mainFormDel").validate({                
                        submitHandler: function() {                    
                             $.ajax({
                                type: "POST",
                                url: "manage-detail-del-save.php",
                                data: $("#mainFormDel").serialize(),
                                success: function(result) {     
                                    if(result.status == '1'){
                                       swal("ลบเสร็จสมบูรณ์ !!!", result.msg, "success")
                                       .then((value) =>{
                                           window.location = 'manage.php';
                                       })                                  
                                    }else{
                                        swal("ผิดพลาด", result.msg, "error");
                                    }
                                },
                                error: function(xhr, status, error){
                                    $.LoadingOverlay("hide");
                                    var msg = xhr.responseText;
                                    swal("มีข้อมูลผิดพลาด", "ไม่สามารถดำเนินการลบข้อมูลได้ กรุณาตรวจสอบ<br>Error Log:<br>" + msg.substring(0,250), "error");
                                }
                             });
                        }
                }); 

                // --------------------- ทำการลบข้อมูล ผลคะแนน  -----------------------
                $( "#mainFormDelScore").validate({                
                        submitHandler: function() {                    
                             $.ajax({
                                type: "POST",
                                url: "manage-detail-del-score-save.php",
                                data: $("#mainFormDelScore").serialize(),
                                success: function(result) {     
                                    if(result.status == '1'){
                                       swal("ลบเสร็จสมบูรณ์ !!!", result.msg, "success")
                                       .then((value) =>{
                                           location.reload();
                                       })                                  
                                    }else{
                                        swal("ผิดพลาด", result.msg, "error");
                                    }
                                },
                                error: function(xhr, status, error){
                                    $.LoadingOverlay("hide");
                                    var msg = xhr.responseText;
                                    swal("มีข้อมูลผิดพลาด", "ไม่สามารถดำเนินการลบข้อมูลได้ กรุณาตรวจสอบ<br>Error Log:<br>" + msg.substring(0,250), "error");
                                }
                             });
                        }
                });      

                 // ----------------------------------------- กรอกเฉพาะตัวเลข ----------------------------------------------
                $(".num").on("keypress" , function (e) {		
                    var code = e.keyCode ? e.keyCode : e.which;                   
                    if(code > 57 ){
                        return false;
                    }else if( code < 48 && code != 8){
                        return false;
                    }	
                });                

                // set datepicker thai --------------------------            
                $.datepicker.regional['th'] ={
                        changeMonth: true,
                        changeYear: true,
                        //defaultDate: GetFxupdateDate(FxRateDateAndUpdate.d[0].Day),
                        yearOffSet: 543,
                        //buttonImageOnly: true,
                        dateFormat: 'dd-mm-yy',
                        dayNames: ['อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'],
                        dayNamesMin: ['อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส'],
                        monthNames: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
                        monthNamesShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                        constrainInput: true,
                        prevText: 'ก่อนหน้า',
                        nextText: 'ถัดไป',
                        yearRange: '-1:+5', //sohks]y
                        buttonText: 'เลือก',
                };
                $.datepicker.setDefaults($.datepicker.regional['th']);                
                $( ".selector" ).datepicker( $.datepicker.regional["th"] ); // Set ภาษาที่เรานิยามไว้ด้านบน

                // -------------------------------------- แก้ไขข้อมูลใหม่ ---------------------------------------------    
                $('#myModalEdit').on('show.bs.modal', function (event){
                    var button = $(event.relatedTarget) 
                    var title = button.data('title');
                    var examdate = button.data('examdate');
                    var maxscore = button.data('maxscore');
                    var publishdate = button.data('publishdate');
                    var status = button.data('status');
                    $('#txtTitle').val(title); 
                    //$('#txtExamDate').val(examdate);   
                    $('#txtMaxScore').val(maxscore);
                    $('#txtPublishDate').val(publishdate);
                    $('#selStatus').val(status);
                    $( "#txtExamDate" ).datepicker("setDate", examdate); //Set ค่าวันปัจจุบัน                    
                });  

               $( "#mainFormEdit").validate({                        
                        ignore: [],
                        debug: false,
                        rules: { 
                            txtScore:{
                                number: true
                            }                     
                       },
                        submitHandler: function() {                    
                             $.ajax({
                                type: "POST",
                                url: "manage-detail-edit-save.php",
                                data: $("#mainFormEdit").serialize(),
                                success: function(result) {     
                                    if(result.status == '1'){
                                       swal("แก้ไขเสร็จสมบูรณ์ !!!", result.msg, "success")
                                       .then((value) =>{
                                            location.reload();
                                       })                                  
                                    }else{
                                        swal("ผิดพลาด", result.msg, "error");
                                    }
                                }
                             });
                        }
                });                
                // -------------------------------------- แก้ไขข้อมูล ---------------------------------------------          

                Chartist.Pie('#chartPass', {
                    labels: ['<?php echo $percentpass.'%' ?>', '<?php echo $percentfail.'%' ?>', '<?php echo $percentno.'%' ?>'],
                    series: [<?php echo $arrPass['pass'] ?>, <?php echo $arrPass['fail'] ?>, <?php echo $arrPass['no'] ?>]
                });

                
                Chartist.Pie('#chartView', {
                    labels: ['<?php echo $percentview.'%' ?>', '<?php echo $percentnotview.'%' ?>'],
                    series: [<?php echo $arrView['view'] ?>, <?php echo $arrView['no'] ?>]
                });                

        </script>

</html>
