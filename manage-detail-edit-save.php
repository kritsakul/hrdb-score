<?PHP

require_once 'redirect.inc.php';
require_once 'cookie.inc.php';
require_once 'function.inc.php';

header("Content-type:application/json; charset=UTF-8");

$daylimit = 89; //วันที่ยกเลิกการประกาศผล
$title =   trim(filter_input(INPUT_POST, 'txtTitle' ));
$examdate = datethaitointer(trim(filter_input(INPUT_POST, 'txtExamDate' )));  //แปลงเป็น yyyy-mm-dd
$publishdate = "";

    if(trim(filter_input(INPUT_POST, 'txtPublishDate' )) !== ""){ //ถ้ามีการเลือก publishdate จะมีการสร้าง unpublishdate(วันหมดอายุ 90 วัน)
        $publishdate = datethaitointer(trim(filter_input(INPUT_POST, 'txtPublishDate' )));
        $unpublishdate = date('Y-m-d',strtotime($publishdate . "+".$daylimit." days"));

        //หากเลือกวันประกาศผล ก่อนหน้าวันที่สอบ
        if($publishdate < $examdate AND $publishdate !== ""){
            $arr = array(
                "status"=>'0',
                "msg"=>'ไม่สามารถบันทีกได้เนื่องจาก ท่านเลือกวันประกาศผล อยู่ก่อนหน้าวันสอบ กรุณาตรวจสอบ'
            );
            echo json_encode($arr);
            exit();
        }
        
    }else{
        $unpublishdate = "";
    }
    
$maxscore = trim(filter_input(INPUT_POST, 'txtMaxScore' ));
$status = trim(filter_input(INPUT_POST, 'selStatus' ));
$roundno =   trim(filter_input(INPUT_POST, 'txtRoundNo' ));
$date = date("Y-m-d H:i:s");

if($title == "" || $examdate == "" || $maxscore == ""  || $status == "" ){ 
    $arr = array(
        "status"=>'0',
        "msg"=>'ไม่สามารถบันทีกได้เนื่องจากกรอกข้อมูลไม่ครบ กรุณาตรวจสอบ'
    );
    echo json_encode($arr);
    exit(); 
}

$stmt = $conn->prepare("                
    UPDATE score_round SET
        round_title=:title,
        round_exam_date=:examdate,
        round_max_score=:maxscore,
        round_publish_date=:publish,
        round_unpublish_date=:unpublish,
        round_updated_date=:updated,
        username=:username,
        round_status=:status
    WHERE
        round_no=:roundno
");

$stmt->bindParam(':title', $title);
$stmt->bindParam(':examdate', $examdate);
$stmt->bindParam(':maxscore', $maxscore);

//ถ้าไม่มีการใส่วันประกาศผล
if($publishdate !== ""){
    $stmt->bindParam(':publish', $publishdate);
    $stmt->bindParam(':unpublish', $unpublishdate);
}else{
    $stmt->bindValue(':publish', null, PDO::PARAM_INT);
    $stmt->bindValue(':unpublish', null, PDO::PARAM_INT);    
    $status = 0; //ถ้าไม่มีการใส่วันประกาศ สถานะก็จะเป็น ปิดประกาศ
}

$stmt->bindParam(':updated', $date);
$stmt->bindParam(':username', $tmpMainUsername);
$stmt->bindParam(':status', $status);
$stmt->bindParam(':roundno', $roundno);
$stmt->execute();
 
$arr = array(
    "status"=>'1',
    "msg"=>'ทำการแก้ไขข้อมูลการประชุม เสร็จเรียบร้อย'
);

echo json_encode($arr);

?>