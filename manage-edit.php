<!doctype html>
<html lang="en">
    <head>
        
        <?php        
            require_once 'connections/conn.php';
            require_once 'cookie.inc.php';
            require_once 'redirect.inc.php';

            //เช็คข้อมูล activity ว่ามีข้อมูลหรือไม่
            //เช็คว่าผู้ใช้เป็นผู้สร้าง การประชุมนี้หรือไม่ 
            //ยกเว้น permission = 1 หมายถึงเป็น admin จะสามารถเข้าดูได้ทั้งหมด
            $tmpActID = filter_input(INPUT_GET, 'actid');    
            $sql = "SELECT act_no 
                FROM 
                    meet_activity
                WHERE 
                    act_id='$tmpActID' AND
                    username = '$tmpMainUsername'     
            ";   
            $query = $conn->prepare($sql);
            $query->execute();
            if ($query->rowCount() == 0 && $tmpMainPermission != '1') {
                header("Location: manage.php");
                exit();
            } else { 
                
                //ถ้าเป็นผู้สร้าง หรือ เป็น admin จะสามารถแก้ไขได้
                $sql2 = "SELECT * 
                    FROM 
                        meet_activity
                    WHERE 
                        act_id='$tmpActID' 
                ";
                $query2 = $conn->prepare($sql2);
                $query2->execute();
                $result = $query2->fetch(PDO::FETCH_ASSOC);    
            }
            
        ?>
        
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>แก้ไข/เปลี่ยนสถานะข้อมูลการประชุม - ระบบลงทะเบียนออนไลน์</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>

        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />

        <!--     Fonts and icons     -->
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

        <!-- Date Picker -->
        <link rel="stylesheet" href="vendors/datepicker/datepicker3.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="vendors/daterangepicker/daterangepicker.css">  
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        
        <style>
            .header{
                padding:20px !important;
            }
           th{
                font-size:16px;
            }
            .font16 .control-label{
                font-size:16px;
                color:#000;
            }
            ::placeholder {
                color:red;
            }
            .error{
                color:#FF0000 !important;
            }
            .middle{
                vertical-align:middle;
            }
            .text-red{
                color:#F00 !important;
            }
        </style>
        
    </head>
    <body>

        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">
                        <div class="row">                       
                            <div class="col-md-12">
                                
                                <div class="card">

                                    <p style="font-size:24px; padding:20px 10px 0px 20px;" class="title">
                                        <i class="pe-7s-note2"></i> <u><a href="manage.php" style="color:#986eda;"> จัดการข้อมูลการประชุม</a></u> > 
                                        <u><a href="manage-detail.php?actid=<?php echo $tmpActID ?>" style="color:#986eda;"> จัดการข้อมูล</a></u> > 
                                        แก้ไข/เปลี่ยนสถานะ <a style="color:#986cda; font-size:14px;" target="_blank" href="detail.php?actid=<?=$tmpActID?>"><u>(ดูตัวอย่างหน้าลงทะเบียน)</u></a>
                                    </p>      
                                    
                                    <hr>
                                    
                                    <div class="content" style="margin-top:50px; padding-bottom:50px;">
                                         
                                                <!-- form start -->
                                                <form id="mainForm" class="form-horizontal">
                                                    <div class="box-body font16">
                                     
                                                        <input type="hidden" id="txtUsername" name="txtUsername" value="<?php echo $tmpMainUsername ?>">

                                                        <div class="form-group">
                                                            <label for="inputEmail3" class="col-sm-3 control-label text-red">*ชื่อเรื่อง</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" maxlength="500"  class="form-control font16 required" required  id="txtTitle" name="txtTitle" placeholder="ชื่อเรื่อง" autocomplete="off" value="<?php echo $result['act_title'] ?>" >
                                                            </div> 
                                                        </div>                                

                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red">*กลุ่มเป้าหมาย</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16 " id="txtTargetGroup" name="txtTargetGroup" required maxlength="150" value="<?= $result['act_target_group']; ?>" placeholder="กลุ่มเป้าหมาย เช่น รพ.สต. ทุกแห่ง, อำเภอเมืองราชบุรี, อำเภอสวนผึ้ง, โรงพยาบาล, ผู้ที่ใช้งานระบบ...">
                                                            </div>
                                                        </div>                     

                                                        <?php
                                                            $tmpStartRegister = $result['act_start_register'];
                                                            $tmpArrStartRegister = explode("-", $tmpStartRegister);
                                                            $tmpStartRegisterShow = $tmpArrStartRegister[2] . "-" . $tmpArrStartRegister[1] . "-" . $tmpArrStartRegister[0];

                                                            $tmpStopRegister = $result['act_stop_register'];
                                                            $tmpArrStopRegister = explode("-", $tmpStopRegister);
                                                            $tmpStopRegisterShow = $tmpArrStopRegister[2] . "-" . $tmpArrStopRegister[1] . "-" . $tmpArrStopRegister[0];
                                                        ?> 
                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red">*ระยะเวลา เปิดให้ลงทะเบียน</label>
                                                            <div class="col-sm-4">                                    
                                                                <input type="text" class="form-control font16  pull-right selector" id="txtStartRegister" required autocomplete="off" name="txtStartRegister" value="<?= $tmpStartRegisterShow; ?>"  placeholder="วันที่ เปิด ลงทะเบียน"> 
                                                            </div>
                                                            <div class="col-sm-4">                                    
                                                                <input type="text" class="form-control font16  pull-right selector" id="txtStopRegister" required autocomplete="off" name="txtStopRegister" value="<?= $tmpStopRegisterShow; ?>"  placeholder="วันที่ ปิด การลงทะเบียน">
                                                            </div>                                
                                                        </div>                         


                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red">*กลุ่มงานที่รับผิดชอบ</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16 " id="txtDepartment" name="txtDepartment" required maxlength="100"  placeholder="กลุ่มงานทีรับผิดชอบ" value="<?= $result['act_department']; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red">*ชื่อ-นามสกุล ผู้รับผิดชอบ</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16 " id="txtManager" name="txtManager" required maxlength="250"   placeholder="ชื่อ-นามสกุล ผู้รับผิดชอบ" value="<?= $result['act_manager']; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red">*อีเมลล์</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16 " id="txtEmail" required name="txtEmail" maxlength="45" placeholder="อีเมลล์" value="<?= $result['act_email']; ?>">
                                                                <input type="hidden" class="form-control font16 " id="txtActID" name="txtActID" required  value="<?= $tmpActID; ?>" >
                                                            </div>
                                                        </div>                                                             

                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red">*เบอร์ติดต่อ</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16" id="txtTel" name="txtTel" required maxlength="40" placeholder="กรอกเบอร์ติดต่อ เช่น 0-748864-000 หรือ 032-123456 ต่อ 1588" autocomplete="off" value="<?= $result['act_tel']; ?>">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label">แฟกซ์ (ถ้ามี)</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16" id="txtFax" name="txtFax" maxlength="30" placeholder="กรอกเฉพาะตัวเลข เช่น 07-4886-4000" autocomplete="off" value="<?= $result['act_fax']; ?>">
                                                            </div>
                                                        </div>                                                        
                                                        
                                                        <hr>
                                                        
                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label  text-red">*เลี้ยงอาหาร</label>
                                                            <div class="col-sm-8">
                                                                <select id="selFood" name="selFood" class="form-control" style="cursor:pointer;" required>
                                                                    <option value=""></option>
                                                                    <option value="0">ไม่มี</option>
                                                                    <option value="1">มี</option>
                                                                </select>
                                                                <span style="color:red;">*หากมีการเลี้ยงอาหาร จะมีหมวดหมู่การรับประทานอาหารให้เลือก (ทั่วไป, อิสลาม, มังสวิรัติ)</span>
                                                            </div>
                                                        </div>   
                                                        
                                                       <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label">รายละเอียดเพิ่มเติม (ถ้ามี)</label>
                                                            <div class="col-sm-8">
                                                                <textarea class="form-control font16 ckeditor"  maxlength="500" id="txtDesc" name="txtDesc" rows="6"   placeholder="รายละเอียดอื่นๆ"><?= $result['act_desc']; ?></textarea> 
                                                            </div>
                                                        </div>                                                        
                                                        
                                                        <hr>

                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label text-red" id="divChangeStatus">สถานะ</label>
                                                            <?php
                                                            //status สถานะ 0ยกเลิก/ลบ 1เปิดลงทะเบียน 2ปิดลงทะเบียน 3เสร็จสมบูรณ์
                                                            $tmpStatus = $result['act_status'];
                                                            if ($tmpStatus == 5) {
                                                                $tmpStatusShow = "ยกเลิกการจัดประชุม";
                                                            } else if ($tmpStatus == 1) {
                                                                $tmpStatusShow = "เปิดลงทะเบียน";
                                                            } else if ($tmpStatus == 2) {
                                                                $tmpStatusShow = "ปิดการลงทะเบียน";
                                                            } else if ($tmpStatus == 3) {
                                                                $tmpStatusShow = "กิจกรรมเสร็จสมบูรณ์";
                                                            } else if ($tmpStatus == 4) {
                                                                $tmpStatusShow = "ยังไม่เปิดให้ลงทะเบียน";
                                                            }
                                                            ?>

                                                            <div class="col-sm-8">
                                                                <select id="selStatus" name="selStatus" class="form-control font16">
                                                                    <option value="<?= $tmpStatus ?>"><?= $tmpStatusShow ?></option>
                                                                    <?php
                                                                    //status สถานะ 0ยกเลิก/ลบ 1เปิดลงทะเบียน 2ปิดลงทะเบียน 3เสร็จสมบูรณ์                                                                    
                                                                    if ($tmpStatus <> 1) {
                                                                        echo "<option value='1' class='text-success'>เปิดลงทะเบียน</option>";
                                                                    }
                                                                    if ($tmpStatus <> 2) {
                                                                        echo "<option value='2' class='text-danger'>ปิดการลงทะเบียน</option>";
                                                                    }
                                                                    if ($tmpStatus <> 3) {
                                                                        echo "<option value='3' class='text-primary'>กิจกรรมเสร็จสมบูรณ์</option>";
                                                                    }
                                                                    if ($tmpStatus <> 4) {
                                                                        echo "<option value='4' class='text-orange'>ยังไม่เปิดให้ลงทะเบียน</option>";
                                                                    }
                                                                    if ($tmpStatus <> 5) {
                                                                        echo "<option value='5' class='text-orange'>ยกเลิกการจัดประชุม</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>                                                           
                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label">หมายเหตุ</label>
                                                            <div class="col-sm-8">
                                                                <input type="text" class="form-control font16 " id="txtComment" name="txtComment" maxlength="1000" value="<?= $result['act_comment']; ?>" placeholder="หมายเหตุ">
                                                            </div>
                                                        </div> 
                                                    <div class="form-group">
                                                        <label for="inputPassword3" class="col-sm-3 control-label"></label>
                                                        <div class=" col-sm-8" style="font-size:12px;">
                                                            <span style="color:red;">* หากเปลี่ยนสถานะเป็น <u>"ยกเลิกการจัดประชุม"</u> ควรจะมีหมายเหตุระบุด้วย ว่าเพราะเหตุใด เช่น ห้องเต็ม, เลื่อนไม่มีกำหนด, มีปัญหาด้านวิทยากร เป็นต้น</span><br>
                                                            <span style="color:red;">** ท่านสามารถสั่งให้ระบบ <u>"ปิดการลงทะเบียน"</u> ได้ ตามความต้องการ เช่น ระบบยังไม่ถึงกำหนดเวลาการลงทะเบียน แต่กลุ่มเป้าหมายเราเกินแล้ว ก็สามารถสั่งปิดการลงทะเบียนได้</span><br>
                                                            <span style="color:red;">*** ถึงจะเปลี่ยนสถานะเป็น <u>"ปิดการลงทะเบียน หรือ ยกเลิกการจัดประชุม"</u> ไปแล้ว ท่านก็สามารถมาสั่งให้ระบบ <span style="color:green;"><u>"เปิดการลงทะเบียน"</u></span> ได้</span><br>
                                                        </div>     
                                                    </div>                                                        

                                                    <!-- /.box-body -->
                                                    <div class="box-footer"> 
                                                        <div class="row-fluid">
                                                            <div class="col-md-8 col-sm-12 col-md-offset-3">
                                                                <button id="cmdSave" type="submit" class="btn btn-lg btn-block btn-success" style="background-color:#986eda; border-color:#986eda; color:#FFF;">บันทึกข้อมูล</button>     
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /.box-footer -->
                                                    <div class="clearfix"></div>
                                             </form>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                            
                            </div>
                        </div>
                    </div>
                </div>


                <?php
                    require_once 'footer.inc.php';
                ?>

            </div>
        </div>


    </body>

        <!--   Core JS Files   -->
        <!-- jQuery 2.2.3 -->
        <!--<script src="vendors/jquery/jquery-2.2.3.min.js"></script>-->
        <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
        <script src="vendors/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
        
        <!-- Bootstrap 3.3.7 -->
        <script src="vendors/bootstrap/js/bootstrap.js" type="text/javascript"></script>
        <script src="vendors/input-mask/jquery.inputmask.js"></script>
        
        <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
        <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

        <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
        <script src="assets/js/demo.js"></script>

        <!-- jquery validation -->
        <script src="vendors/jquery-validation/dist/jquery.validate.js"  type="text/javascript"></script>     
        <script src="vendors/jquery-validation/dist/localization/messages_th.js"  type="text/javascript"></script>    

        <!--Custom ckeditor -->
        <script src="vendors/ckeditor/ckeditor.js" type="text/javascript"></script>    
        
        <?php
            $tmpFood = $result['act_food'];
            if($tmpFood == ""){
                $tmpFood = 0;
            }
        ?>
        
        <script>                      
            
                
                $(document).ready(function(){
                     $('#selFood').val('<?php echo $tmpFood ?>');
                })
            
                // -------------------- บันทึกข้อมูล -------------------
                $.validator.setDefaults({
                    submitHandler: function(){                        
                         $.ajax({
                            type: "POST",
                            url: "manage-edit-save.php",
                            data: $("#mainForm").serialize(),
                            success: function(result) {     
                                if(result.status == '1'){
                                    swal("เสร็จสมบูรณ์ !!!", result.msg, "success")
                                }else{
                                    swal("ผิดพลาด !!!", result.msg, "error");
                                }
                            }
                         });
                    }
                });               
                $( "#mainForm").validate({
                     ignore: [],
                     debug: false,
                         rules: { 
                             txtEmail:{
                                 email: true
                             }
                        }
                });


                //deal with copying the ckeditor text into the actual textarea
                CKEDITOR.on('instanceReady', function () {
                    $.each(CKEDITOR.instances, function (instance) {
                        CKEDITOR.instances[instance].on("keyup", CK_jQ);
                        CKEDITOR.instances[instance].on("paste", CK_jQ);
                        CKEDITOR.instances[instance].on("keypress", CK_jQ);
                        CKEDITOR.instances[instance].on("blur", CK_jQ);
                        CKEDITOR.instances[instance].on("change", CK_jQ);
                        CKEDITOR.instances[instance].on("click", CK_jQ);
                    });
                });
                function CK_jQ() {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                    }
                }            
            
                $( function() {
                    $( ".selector" ).datepicker({
                        showAnim: "clip",
                        dateFormat: "dd-mm-yy"
                    });
                });            
                $("[data-mask]").inputmask();
                
                // -------------------- กรอกเฉพาะตัวเลข -------------------
                $(".num").on("keypress" , function (e) {		
                    var code = e.keyCode ? e.keyCode : e.which;                   
                    if(code > 57 ){
                        return false;
                    }else if( code < 48 && code != 8){
                        return false;
                    }		
                });      

        </script>

</html>
