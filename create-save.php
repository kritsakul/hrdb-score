<?PHP


require_once 'redirect.inc.php';
require_once 'cookie.inc.php';
require_once 'function.inc.php';

header("Content-type:application/json; charset=UTF-8");
$title =   trim(filter_input(INPUT_POST, 'txtTitle' ));
$examdate = datethaitointer(trim(filter_input(INPUT_POST, 'txtExamDate' )));  //แปลงเป็น yyyy-mm-dd
$maxscore = trim(filter_input(INPUT_POST, 'txtMaxScore' ));
$date = date("Y-m-d H:i:s");

if($title == "" || $examdate == "" || $maxscore == "" ){ 
        $arr = array(
            "status"=>'0',
            "msg"=>'ไม่สามารถบันทีกได้เนื่องจากกรอกข้อมูลไม่ครบ กรุณาตรวจสอบ'
        );
        echo json_encode($arr);
        exit(); 
}

$stmt = $conn->prepare("                
    INSERT INTO score_round(
        round_title,
        round_exam_date, 
        round_max_score,
        round_created_date,
        username
    )VALUES(
        :title,
        :examdate,
        :maxscore,
        :created,
        :username      
     )  
");

$stmt->bindParam(':title', $title);
$stmt->bindParam(':examdate', $examdate);
$stmt->bindParam(':maxscore', $maxscore);
$stmt->bindParam(':created', $date);
$stmt->bindParam(':username', $tmpMainUsername);
$stmt->execute();
 
$arr = array(
    "status"=>'1',
    "msg"=>'ทำการเพิ่มข้อมูลการประชุม เสร็จเรียบร้อย'
);

echo json_encode($arr);

?>