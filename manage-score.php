<!doctype html>
<html lang="en">
    <head>
        
        
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <title>จัดการข้อมูลผลคะแนน - กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข</title>

        <?php
        
            require_once 'redirect.inc.php';  
            require_once 'function.inc.php';   
            require_once 'header.inc.php'; 
            
            $roundno = filter_input(INPUT_GET, 'roundno');  
            $sql = "SELECT * FROM score_round WHERE round_no=:roundno LIMIT 0,1";
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':roundno', $roundno);
            $stmt->execute();

            //ถ้าไม่มีข้อมูล actid จะไม่ redirect ไปหน้าหลัก
            if ($stmt->rowCount() <= 0) {
                header("Location: index.php"); /* Redirect browser */
                exit();
            } else {
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
            } 
                        
            $roundno = $result['round_no'];
            $title = $result['round_title'];
            $examdate = $result['round_exam_date'];
            $maxscore = $result['round_max_score'];
            $publishdate = $result['round_publish_date'];
            if($publishdate == ""){
                $publishdate = "ยังไม่ประกาศ";
            }
            $status = $result['round_status'];
            if($status == 0){
                $statusshow = '<span style="color:red">ปิดประกาศ</span>';
            }else{
                $statusshow = '<span style="color:green">กำลังประกาศผล</span>';
            }
            
        ?>        
                
        <style>
            .header{
                padding:20px !important;
            }
           th{
                color:#000;
                font-size:16px;
            }
            .td-middle{
                vertical-align:middle;
            }
            td{
                vertical-align:top !important;
            }
            label{
                font-size:20px !important;
                color:#000 !important;
            }              
            th{
                background-color:#66cc00 !important;
                color:#FFF !important;
            }
        </style>
        
    </head>
    <body>
        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">                                         
                        <div class="card">  

                            <p style="font-size:24px; padding:20px 10px 0px 20px;" >
                                <i class="pe-7s-note2"></i> 
                                <u><a href="manage.php" style="color:green;"> จัดการข้อมูลประกาศผลคะแนน</a></u> > 
                                <u><a href="manage-detail.php?roundno=<?php echo $roundno ?>" style="color:green;"> จัดการข้อมูล</a></u> > 
                                จัดการข้อมูลผลคะแนน
                            </p>   

                            
                            <hr style="margin-bottom:0px;">

                            <div class="row-fluid" style="padding-bottom:20px;">
                                    
                                    <div class="col-xs-12" style="text-align:right; margin-top:10px;">              
                                        <button  type="button" class="btn btn-fill btn-info" id="btnImport" role="button" data-target="#modalimport" data-toggle="modal">
                                            <i class="fa fa-file-excel-o" ></i>&nbsp;อ่านไฟล์ excel
                                        </button>
                                        <button  type="button" class="btn btn-fill btn-danger hide" id="btnDelete" role="button">
                                            <i class="fa fa-ban" ></i>&nbsp;ลบข้อมูลเพื่ออ่านไฟล์ใหม่
                                        </button>      
                                        <button  type="button" class="btn btn-fill btn-success hide" id="btnConfirm" role="button">
                                            <i class="fa fa-save" ></i>&nbsp;บันทึกผลคะแนนเข้าฐานข้อมูล
                                        </button>                                          
                                    </div>

                                    <!-- Small boxes (Stat box) -->
                                    <div class="clearfix"></div>
                                    <div class="row-fluid" style="margin-bottom:5px; margin-top:20px; text-align: center;">
                                        <div class="row-fluid" style="text-align:center;">
                                            <span style="font-size:20px;"><b><?php echo $title ?></span><br>
                                            <span style="font-size:16px;">วันที่สอบ <?php echo fullthaidate($examdate) ?> (คะแนนเต็ม <?php echo $maxscore ?> คะแนน)</span>
                                        </div>    
                                    </div>  
                                    <!-- /.row -->

                                    <div class="container-fluid" style="margin-top:10px;">
                                        <div class="row-fluid">                                        
                                            <div class="table-responsive" style="width:auto;" >                                                    
                                                <form autocomplete="false">
                                                    <table id="tbMain" class="table table-striped table-bordered highlight2"  cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center history" width="10%">เลขประจำ<br>ตัวสอบ</th>
                                                                <th class="text-center" width="20%">ตำแหน่ง</th>
                                                                <th class="text-center" width="10%">คำนำหน้า</th>
                                                                <th class="text-center" width="10%">ชื่อ</th>
                                                                <th class="text-center" width="10%">นามสกุล</th>
                                                                <th class="text-center" width="10%">รหัสประชาชน</th>
                                                                <th class="text-center" width="5%">GPA</th>
                                                                <th class="text-center" width="5%">คะแนนที่ได้</th>  
                                                                <th class="text-center" width="5%">เกณฑ์การตัดสิน</th>
                                                            </tr>                                                
                                                        </thead>
                                                        <tbody>                                                                    
                                                        </tbody>
                                                    </table>                                                
                                                    <div class="text-red">
                                                        *การนำเข้าข้อมูล หากมีข้อมูลก่อนหน้านี้ ระบบจะทำการลบข้อมูลก่อนหน้านี้ทั้งหมด
                                                    </div>
                                                </form>
                                            </div>              
                                        </div>
                                    </div>
                            </div>   
                            
                        </div>
                    </div>
                </div>
            
            <?php
                require_once 'footer.inc.php';                    
            ?>
            
        </div>
        
        
        <!------------------------------------------------------------------------------------------>
        <!------------------------------------ myModal Import  ------------------------------------>
        <!------------------------------------------------------------------------------------------>
        <div class="modal fade" id="modalimport" name="modalimport" role="dialog">            
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <h4 class="modal-title" >นำเข้าข้อมูล</h4>
                    </div>

                    <form id="mainFormImport" class="form-horizontal" accept-charset="UTF-8" method="post">       
                        <input type="hidden" id="txtRoundNo" name="txtRoundNo" value="<?php echo $roundno ?>">
                        <div class="modal-body font16">
                            <div class="form-horizontal form-label-left" style="padding-bottom:2px; ">
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">*เลือกไฟล์</label>
                                    <div class="col-sm-8">
                                        <input type="file"  id="file" name="file">
                                    </div>                                                            
                                </div>                                 
                            </div>  
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-fill btn-default" data-dismiss="modal">ปิด</button>
                            <button type="submit" class="btn btn-fill btn-success" id="btnSaveEdit" style="min-width:200px;">อ่านข้อมูล</button>
                        </div> 
                    </form>

                </div>
            </div>
        </div>         

        
    </body>

    <!--   Core JS Files   -->
    <!--<script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>-->
    <script src="vendors/jquery/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
    <script src="assets/js/demo.js"></script>
    <script src="script.js" type="text/javascript"></script>    
    
    <!-- jquery validation -->
    <script src="vendors/jquery-validation/dist/jquery.validate.js"  type="text/javascript"></script>     
    <script src="vendors/jquery-validation/dist/localization/messages_th.js"  type="text/javascript"></script>        
    
    <!-- loadingoverlay -->
    <script src="vendors/jquery-loading-overlay-master/dist/loadingoverlay.js"></script>
    
    <!-- datatables -->  
    <script src="vendors/datatable/DataTables-1.10.16/js/jquery.dataTables.js"></script>
    <!--<script src="vendors/datatable/Buttons-1.5.1/js/dataTables.buttons.js"></script>
    <script src="vendors/datatable/Buttons-1.5.1/js/buttons.print.js"></script>    
    <script src="vendors/datatable/Buttons-1.5.1/js/buttons.html5.js"></script>
    <script src="vendors/datatable/JSZip-2.5.0/jszip.js"></script>
    <script src="vendors/datatable/pdfmake.min.js"></script>         
    <script src="vendors/datatable/vfs_fonts.js"></script> -->
    
    <script>
        
            $('#tbMain').DataTable({
                 "dom": 'ฺ<"top"f>Brt<"top"ilp>',
                  "responsive": true,
                  "lengthMenu": [[15,  50, 100, -1], [15, 50, 100, "ทั้งหมด"]],
                  "pageLength": 15,
                  "processing": true,
                "columns": [					
                        { "data": "id"},
                        { "data": "position"},
                        { "data": "prename"},
                        { "data": "fname"},
                        { "data": "lname"},
                        { "data": "customerid"},
                        { "data": "gpa"},
                        { "data": "score"},
                        { "data": "result"}
                ],
                "language": {
                      "sProcessing":   "กำลังดำเนินการ...",
                      "sLengthMenu":   "แสดง _MENU_ แถว",
                      "sZeroRecords":  "ไม่พบข้อมูล",
                      "sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                      "sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
                      "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
                      "sInfoPostFix":  "",
                      "sSearch":       "ค้นหา: ",
                      "sUrl":          "",
                      "oPaginate": {
                          "sFirst":    "หน้าแรก",
                          "sPrevious": "ก่อนหน้า",
                          "sNext":     "ถัดไป",
                          "sLast":     "หน้าสุดท้าย"
                      }
                  }
            })                                   
        
            // -------------------- อ่านข้อมูล ------------------- 
           $('#mainFormImport').validate({
                submitHandler: function(form){
                   
                    //var test = $("#txttest").val();
                    //var file_data = $('#file').val();   
                    //$("#txtCopyFile").val(file_data);
                    //var form_data = new FormData();  
                    //form_data.append('test', test);
                    //form_data.append('file', file_data);                   
                    //alert(form_data);
                     $.LoadingOverlay("show");
                     $.ajax({
                        type: "POST",
                        url: "manage-score-read.php",
                        //dataType: 'text',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data:  new FormData(form),
                        success: function(result) {     
                            if(result.status == '1'){
                                $.LoadingOverlay("hide");
                                $('#modalimport').modal('toggle');                                
                                //clear data
                                var table = $('#tbMain').DataTable(); 
                                table
                                    .clear()
                                    .draw();
                                                               
                               //swal("เสร็จสมบูรณ์ !!!", result.json, "success") 
                               var table = $('#tbMain').DataTable();  
                               table.rows.add(result.json).draw();
                               
                                $('#btnImport').addClass('hide');
                                $('#btnConfirm').removeClass('hide');
                                $('#btnDelete').removeClass('hide');
                               
                            }else{
                                $.LoadingOverlay("hide");
                                swal("มีข้อมูลผิดพลาด", "ไม่สามารถอ่านไฟล์ได้ กรุณาตรวจสอบ นามสกุลไฟล์ถูกต้องหรือไม่, มีชื่อคอลัมน์ ตรงตามแบบฟอร์มหรือไม่", "error");
                                
                            }
                        },
                        error: function(xhr, status, error){
                            $.LoadingOverlay("hide");
                            var msg = xhr.responseText;
                            swal("มีข้อมูลผิดพลาด", "ไม่สามารถอ่านไฟล์ได้ กรุณาตรวจสอบ นามสกุลไฟล์ถูกต้องหรือไม่, มีชื่อคอลัมน์ ตรงตามแบบฟอร์มหรือไม่<br>" + msg.substring(0,250), "error");
                        }
                     });
                }                    
            });       
            
            //------------- เคลียร์ข้อมูล -------------------
            $('#btnDelete').click(function(){
                var table = $('#tbMain').DataTable(); 
                table
                    .clear()
                    .draw();            
                $('#file').val('');
                $('#btnImport').removeClass('hide');
                $('#btnConfirm').addClass('hide');
                $('#btnDelete').addClass('hide');                    
            })
           
           // -------------- บันทึกข้อมูลการนำเข้า ----------------- 
            $('#btnConfirm').click(function(){        
                $('#btnConfirm').prop( "disabled", true );
                swal({
                    title: "ยืนยันการนำเข้าฐานข้อมูล?",
                    text: "คุณต้องการนำเข้าฐานข้อมูลใช่หรือไม่\nหากมีข้อมูลก่อนหน้านี้ระบบจะทำการลบทั้งหมด\nกรุณายืนยัน",
                    icon: "warning",
                    buttons: true,
                    dangerMode: false,
                })
                .then((willsave) => {
                    if (willsave){
                                                
                        $.LoadingOverlay("show")   
                        var roundno = $("#txtRoundNo").val();                        
                        //var filedata = $('#file').files[0];
                        var filedata = $('#file').prop('files')[0];
                        var form_data = new FormData();  
                        form_data.append('roundno', roundno);
                        form_data.append('file', filedata);                        
                                       
                        $.ajax({
                            type: "POST",
                            url: "manage-score-save.php",
                            data:form_data,
                            cache: false,
                            contentType: false,
                            processData: false,
                           success: function(result) {    
                               if(result.status == '1'){           
                                  $.LoadingOverlay("hide");
                                  window.location='manage-detail.php?roundno='+roundno;                     
                               }else{
                                   $.LoadingOverlay("hide");
                                   $('#btnConfirm').prop( "disabled", false),
                                   swal("มีข้อมูลผิดพลาด", result.msg , "error");
                               }
                           },
                            error: function(xhr, status, error){
                                $.LoadingOverlay("hide");
                                var msg = xhr.responseText;
                                swal("มีข้อมูลผิดพลาด", "ไม่สามารถอ่านไฟล์ได้ กรุณาตรวจสอบ นามสกุลไฟล์ถูกต้องหรือไม่, มีชื่อคอลัมน์ ตรงตามแบบฟอร์มหรือไม่<br>" + msg.substring(0,250), "error");
                            }
                        }); 
                        
                     }else{
                        $('#btnConfirm').prop( "disabled", false)
                      
                    }
                });              
            })                               

                       
            // -------------------- บันทึกข้อมูล -------------------
            /*
            $.validator.setDefaults({
                submitHandler: function() {                  
                     $('#cmdSave').prop( "disabled", true ),
                     $.ajax({
                        type: "POST",
                        url: "create-save.php",
                        data: $("#mainForm").serialize(),
                        success: function(result) {     
                            if(result.status == '1'){                                  
                               swal("เสร็จสมบูรณ์ !!!", result.msg, "success")
                               .then((value) =>{
                                    window.location='manage.php';
                               })                                  
                            }else{
                                $('#cmdSave').prop( "disabled", false),
                                swal("ผิดพลาด", result.msg, "error");
                            }
                        }
                     });
                }
            });       

            $( "#mainForm").validate({
                 ignore: [],                    
                 debug: false,
                     rules: { 
                         txtEmail:{
                             email: true
                         }
                    }
            });     
       */
           
    </script>
    
</html>
