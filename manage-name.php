<!doctype html>
<html lang="en">
    <head>
        
        <?php
            require_once 'connections/conn.php';
            require_once 'cookie.inc.php';
            require_once 'redirect.inc.php';
            require_once 'function.inc.php';
            
            $tmpActID = filter_input(INPUT_GET, 'actid');    
            $tmpGenNo = filter_input(INPUT_GET, 'genno');    
            $tmpList = filter_input(INPUT_GET, 'list'); //list = status
  
            //เช็คว่า หน้านี้มีรหัส tmpActID ถูกต้องหรือไม่
            $sql = "SELECT * FROM meet_activity WHERE act_id='$tmpActID'";
            $query = $conn->prepare($sql);
            $query->execute();
            if ($query->rowCount() == 0) { //ถ้าไม่ถูกต้องจะกลับไปที่หน้าหลัก
                header("Location: index.php");
                exit();
            } else {
                $result = $query->fetch(PDO::FETCH_ASSOC);
                $tmpActTitle = $result['act_title'];
                $tmpStatus = $result['act_status'];               
                $tmpStartRegister = $result['act_start_register'];
                $tmpStopRegister = $result['act_stop_register'];
                
                //เช็คสถานะ 0ยกเลิก/ลบ 1เปิดลงทะเบียน 2ปิดลงทะเบียน 3เสร็จสมบูรณ์, 4ยังไม่เปิดให้ลงทะเบียน
                $tmpRegStatus = $result['act_status']; 
            }                                                                
                  
            //เช็คการแจ้งเตือน โดยเก็บอยู่ในรูปแบบ Array ว่ามีผู้ใช้งานท่านใดแจ้งเตือนไปบ้าง ก็จะนำ Array ไปเช็คกับ reg_id ถ้าเจอจะมีสัญลักษณ์บอก
            $sql = "SELECT 
                    meet_register_alert.reg_id 
                FROM 
                    meet_register_alert, meet_register
                WHERE 
                    meet_register_alert.reg_id = meet_register.reg_id AND
                    meet_register.act_id='$tmpActID' AND
                    meet_register.gen_no = '$tmpGenNo' AND
                    meet_register_alert.alert_status = 0   
                GROUP BY
                    meet_register_alert.reg_id 
            ";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $tmpCountAlert = $stmt->rowCount();
            if ($tmpCountAlert > 0) { //ถ้าพบรายการแจ้งเตือน
                while ($rsAlert = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $arrAlert[] = $rsAlert['reg_id'];
                }
            } else {
                $arrAlert[] = "";
            }
            
            $thaiShortMonthArr = array(
                "01" => "ม.ค.",
                "02" => "ก.พ.",
                "03" => "มี.ค.",
                "04" => "เม.ย.",
                "05" => "พ.ค.",
                "06" => "มิ.ย.",
                "07" => "ก.ค.",
                "08" => "ส.ค.",
                "09" => "ก.ย.",
                "10" => "ต.ค.",
                "11" => "พ.ย.",
                "12" => "ธ.ค."
            );   
            
            $sql = "SELECT * FROM meet_activity_gen WHERE act_id='$tmpActID' AND gen_no='$tmpGenNo' ORDER BY gen_index";
            $query = $conn->prepare($sql);
            $query->execute();
                $result = $query->fetch(PDO::FETCH_ASSOC);
                $tmpIndex = $result['gen_index'];
                $tmpPlace = $result['gen_place'];
                $tmpStartDate = $result['gen_start_date'];
                $tmpArrStartDate = explode("-",$tmpStartDate);
                $tmpStartDateShow = $tmpArrStartDate[2] . " " . $thaiShortMonthArr[$tmpArrStartDate[1]] . " " . ($tmpArrStartDate[0] + 543);

                $tmpStopDate = $result['gen_stop_date'];
                $tmpArrStopDate = explode("-",$tmpStopDate);
                $tmpStopDateShow = $tmpArrStopDate[2] . " " . $thaiShortMonthArr[$tmpArrStopDate[1]] . " " . ($tmpArrStopDate[0] + 543);

                $tmpStartTime = substr($result['gen_start_time'],0,5);
                $tmpStopTime = substr($result['gen_stop_time'],0,5);
                $tmpTargetPerson = $result['gen_target_person'];                  
            
        ?>
        
        <meta charset="utf-8" />
        <link rel="icon" type="image/png" href="assets/img/favicon.ico">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>ข้อมูลผู้ลงทะเบียน <?php echo $tmpActTitle ?> - ระบบลงทะเบียนออนไลน์</title>

        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />

        <!-- Bootstrap core CSS     -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

        <!-- Animation library for notifications   -->
        <link href="assets/css/animate.min.css" rel="stylesheet"/>

        <!--  Light Bootstrap Table core CSS    -->
        <link href="assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>

        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="assets/css/demo.css" rel="stylesheet" />

        <!--     Fonts and icons     -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
        <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

        <!-- data table css -->
       <link rel="stylesheet" href="vendors/datatable/datatables.css">
       <link rel="stylesheet" href="vendors/datatable/DataTables-1.10.16/css/dataTables.bootstrap.css">
       <link rel="stylesheet" href="vendors/datatable/Buttons-1.5.1/css/buttons.dataTables.css">           
        
        <style>
            .header{
                padding:20px !important;
            }
           th{
                color:#000;
                font-size:16px;
            }
            .td-middle{
                vertical-align:middle;
            }
            td{
                vertical-align:top !important;
            }
            .text-center{
                text-align:center;
            }
            .font12{
                font-size:12px;
            }
            .font10{
                font-size:10px;
            }   
            .font8{
                font-size:8px;
            }                  
            .text-red{
                color:#F00 !important;
            }
            label{
                font-size:20px !important;
                color:#000 !important;
            }  
        </style>
        
    </head>
    <body>
        <div class="wrapper">

                <?php
                    require_once 'menu.inc.php';
                ?>

                <div class="content">
                    <div class="container-fluid">
                        
                                         
                                <div class="card">                                    
                                    
                                    <p style="font-size:24px; padding:20px 10px 0px 20px;" class="title">
                                        <i class="pe-7s-note2"></i> <u><a href="manage.php" style="color:#986eda;"> จัดการข้อมูลการประชุม</a></u> > 
                                        <u><a href="manage-detail.php?actid=<?php echo $tmpActID ?>" style="color:#986eda;"> จัดการข้อมูล</a></u> > 
                                        จัดการรายชื่อผู้ลงทะเบียน
                                    </p>   

                                    <hr style="margin-bottom:0px;">
                                                                                                                       
                                    <div class="row-fluid" style="padding-bottom:20px;">
                                        
                                            <div class="col-xs-12" style="text-align:right; margin-top:20px;">                                                    
                                                <div class="btn-group btn-group-md ">
                                                    <button  type="button" class="btn btn-fill btn-block btn-info dropdown-toggle" id="drop3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <i class="fa fa-file-excel-o" ></i>&nbsp;ส่งออก ใบลงทะเบียน<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="drop3">
                                                      <li><a class="font20" target="_blank" href="manage-name-excel.php?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>&list=ok">Excel : รายชื่อเฉพาะ อนุมัติ</a></li>
                                                      <li><a class="font20" target="_blank" href="manage-name-excel.php?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>&list=all">Excel : รายชื่อทั้งหมด</a></li>                                                          
                                                    </ul>
                                                </div>      
                                                <a href="manage-name-excel-all.php?actid=<?php echo $tmpActID ?>" class="btn btn-info btn-fill btn-md"><i class="fa fa-file-excel-o" ></i>&nbsp;ส่งออกข้อมูลลงทะเบียนทั้งหมด</a>
                                                <a href="manage-name-report.php?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>" class="btn btn-success btn-fill btn-md"><i class="fa fa-list-alt " ></i>&nbsp;สรุปข้อมูลการลงทะเบียน</a>
                                            </div>                                                

                                            <?php        
                                                //เช็คทั้งหมดก่อน ว่าแต่ละสถานะ มีเท่าไหร่
                                                $sql = "SELECT reg_no,reg_status FROM meet_register WHERE  act_id=:tmpactid AND gen_no=:genno";
                                                $stmt = $conn->prepare($sql);
                                                $stmt->bindParam(':tmpactid', $tmpActID);
                                                $stmt->bindParam(':genno', $tmpGenNo);
                                                $stmt->execute();
                                                $tmpCount = $stmt->rowCount();

                                                //นับจำนวนสถานะ                                
                                                $tmpStatus1 = 0;
                                                $tmpStatus2 = 0;
                                                $tmpStatus0 = 0;
                                                while ($rsCheckStatus = $stmt->fetch(PDO::FETCH_ASSOC)) {
                                                    $tmpStatus = trim($rsCheckStatus['reg_status']);
                                                    switch ($tmpStatus) {
                                                        case 1:
                                                            //รออนุมัติ
                                                            $tmpStatus1++;
                                                            break;
                                                        case 2:
                                                            //อนุมัติแล้ว
                                                            $tmpStatus2++;
                                                            break;
                                                        case 0:
                                                            //ยกเลิก
                                                            $tmpStatus0++;
                                                            break;
                                                    }
                                                }
                                            ?>          

                                            <!-- Small boxes (Stat box) -->
                                            <div class="clearfix"></div>
                                            <div class="row-fluid" style="margin-bottom:5px; margin-top:20px; text-align: center;">                                                    

                                                <div class="row-fluid" style="text-align:center;">
                                                    <p style="font-size:18px;"><b><?php echo $tmpActTitle ?></p>
                                                    <span style="font-size:16px;">วันที่ <?php echo $tmpStartDateShow . " - " . $tmpStopDateShow ?> เวลา <?php echo $tmpStartTime . "น. -" . $tmpStopTime . "น."?></span><br>
                                                    <span style="font-size:16px;"><?php echo $tmpPlace ?></span>
                                                </div>                                                    

                                                <div class="container-fluid" style="margin-top:20px;">
                                                    <div class="font12em">คลิกเพื่อดูรายชื่อแต่ละสถานะ</div>
                                                    <a id="btnList" href="?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>" class="btn btn-outline btn-info  btn-round btn-wd btn-sm"><b>ลงทะเบียนทั้งหมด <?php echo $tmpCount ?> คน</b></a>
                                                    <a id="btnList1" href="?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>&list=1" class="btn btn-outline btn-warning btn-round btn-wd btn-sm"><b>รออนุมัติ <?php echo $tmpStatus1 ?> คน</b></a>
                                                    <a id="btnList2" href="?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>&list=2" class="btn btn-outline btn-success btn-round btn-wd btn-sm"><b>อนุมัติแล้ว <?php echo $tmpStatus2 ?> คน</b></a>
                                                    <a id="btnList0" href="?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>&list=0" class="btn btn-outline btn-danger btn-round btn-wd btn-sm"><b>ไม่อนุมัติ <?php echo $tmpStatus0 ?> คน</b></a>
                                                </div>
                                            </div>  
                                            <!-- /.row -->   

                                            
                                            <?php
                                                $sql = "SELECT gen_no, gen_index FROM meet_activity_gen WHERE act_id='$tmpActID' AND gen_status='1' ORDER BY gen_index";
                                                $query = $conn->prepare($sql);
                                                $query->execute();
                                                if($query->rowCount() > 1){ //ถ้ามีรุ่นมากกว่า 1 ถึงจะให้ทำการเลือก
                                            ?>                                            
                                                <div class="container-fluid" style="margin-top:20px;">
                                                    <select id="selGenNo" style="cursor:pointer;" class="form-control">
                                                            <?php                                                            
                                                                while ($result = $query->fetch(PDO::FETCH_ASSOC)){
                                                                    $tmpIndex = $result['gen_index'];
                                                                    $tmpNo = $result['gen_no'];
                                                                    if($tmpNo == $tmpGenNo){
                                                                        $tmpSelect = "selected";
                                                                    }else{
                                                                        $tmpSelect = "";
                                                                    }
                                                                    echo "<option $tmpSelect value='$tmpNo'>" . "รุ่นที่ " . $tmpIndex . "</option>";
                                                                }
                                                            ?>
                                                    </select>
                                                </div>
                                            <?php
                                                }
                                            ?>

                                            <div class="container-fluid" style="margin-top:10px;">
                                                <div class="row-fluid">                                        
                                                    <div class="table-responsive" style="width:auto;" >

                                                            <table  class="table table-bordered table-hover table-striped" >
                                                                <?php
                                                                //ถ้ามีรายการแก้ไข ล่าสุดที่ยังไม่ได้ ทำการแก้ไข ระบบจะแสดง
                                                                //เช็คจาก alert_status=0 คือยังไม่ได้ทำการแก้ไข
                                                                $sql = "SELECT 
                                                                        meet_register_alert.*
                                                                    FROM 
                                                                        meet_register_alert, meet_register
                                                                    WHERE 
                                                                        meet_register_alert.reg_id = meet_register.reg_id AND
                                                                        meet_register.act_id='$tmpActID' AND
                                                                        meet_register.gen_no = '$tmpGenNo' AND
                                                                        alert_status='0' 
                                                                    ORDER BY alert_no DESC ";
                                                                $rsAlert = $conn->prepare($sql);
                                                                $rsAlert->execute();
                                                                $tmpCountAlert = $rsAlert->rowCount();
                                                                if ($tmpCountAlert > 0) { //ถ้าพบรายการแจ้งเตือน ที่ยังไม่ได้แก้ไข 

                                                                    $tmpShowAlert = "<i class='fa fa-warning text-red'></i>";
                                                                    ?>     

                                                                    <tr class="font16" style='background-color:orange;'>
                                                                        <th colspan="5" class="text-center" style="vertical-align:middle; color:white;">รายการ แจ้งแก้ไข (ที่ยังไม่ได้แก้ไข)</th>
                                                                        <th colspan="2" class="text-center" style="vertical-align:middle; color:white;">แก้ไข</th>  
                                                                    </tr>

                                                                    <?php
                                                                    while ($dataAlert = $rsAlert->fetch(PDO::FETCH_ASSOC)) {
                                                                    ?>

                                                                        <tr>
                                                                            <td colspan="5"><?php echo $dataAlert['alert_detail'] ?></td>
                                                                            <td colspan="2"  class="text-center">
                                                                                <a href="manage-name-edit.php?actid=<?php echo $dataAlert['act_id'] ?>&regid=<?php echo $dataAlert['reg_id'] ?>&genno=<?php echo $tmpGenNo ?>">
                                                                                    <span class="btn btn-fill btn-warning btn-sm">แก้ไข</span>
                                                                                </a>
                                                                            </td>                                          
                                                                        </tr>

                                                                        <?php
                                                                    }
                                                                }
                                                                ?>     
                                                            </table>                                                            

                                                            <input type="hidden" id="txtActID" value="<?php echo $tmpActID ?>">
                                                            <input type="hidden" id="txtGenNo" value="<?php echo $tmpGenNo ?>">

                                                            <table id="tbMain" class="table table-striped table-bordered highlight"  cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center" width='5%'>ลำดับ</th>
                                                                        <th class="text-center" width='7%'>วันลงทะเบียน</th>
                                                                        <th class="text-center" width='25%'>ชื่อ - นามสกุล</th>
                                                                        <th class="text-center" width='25%'>หน่วยงาน</th>
                                                                        <th class="text-center" width='10%'>เบอร์โทร/Email</th>
                                                                        <th class="text-center" width='10%'>สถานะ<br>               
                                                                            <p style="font-size:12px;">คลิกเพื่อเปลี่ยนสถานะ</p>
                                                                            <p id="btnAllSuccess" class="btn btn-fill btn-xs btn-success btn-block" style="width:140px; margin:auto;">อนุมัติทั้งหมด</p>
                                                                        </th>  
                                                                        <th width='5%'></th> 
                                                                    </tr>                                                                    
                                                                </thead>
                                                            </table>                                

                                                    </div>                                         
                                                </div>
                                            </div>

                                    </div>   
                                
                    </div>
                </div>
            </div>
            
            <?php
                require_once 'footer.inc.php';                    
            ?>
            
        </div>      
    </body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>
    <script src="assets/js/demo.js"></script>
    <script src="script.js" type="text/javascript"></script>
    
    
    <!-- datatables -->  
    <script src="vendors/datatable/DataTables-1.10.16/js/jquery.dataTables.js"></script>
<!--    <script src="vendors/datatable/Buttons-1.5.1/js/dataTables.buttons.js"></script>
    <script src="vendors/datatable/Buttons-1.5.1/js/buttons.print.js"></script>    
    <script src="vendors/datatable/Buttons-1.5.1/js/buttons.html5.js"></script>
    <script src="vendors/datatable/JSZip-2.5.0/jszip.js"></script>
    <script src="vendors/datatable/pdfmake.min.js"></script>         
    <script src="vendors/datatable/vfs_fonts.js"></script>               -->
    
    <script>        
        
        // -----------------------------------------------------------------------------------------------------------
        //------------------------------------------- ส่วนของ datatable ------------------------------------------
        // ----------------------------------------------------------------------------------------------------------- 
        $('#tbMain').DataTable({
            "dom": 'ฺ<"top"f>Brt<"top"ilp>',
            "responsive": true,
            "lengthMenu": [[25,  50, 100, -1], [25, 50, 100, "ทั้งหมด"]],
            "pageLength": -1,
            "processing": true,
            "language": {
                "sProcessing":   "กำลังดำเนินการ...",
	"sLengthMenu":   "แสดง _MENU_ แถว",
	"sZeroRecords":  "ไม่พบข้อมูล",
	"sInfo":         "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
	"sInfoEmpty":    "แสดง 0 ถึง 0 จาก 0 แถว",
	"sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
	"sInfoPostFix":  "",
	"sSearch":       "ค้นหา: ",
	"sUrl":          "",
	"oPaginate": {
                    "sFirst":    "หน้าแรก",
                    "sPrevious": "ก่อนหน้า",
                    "sNext":     "ถัดไป",
                    "sLast":     "หน้าสุดท้าย"
	}
            },
            "ajax": {                        
                "url" : "manage-name-data.php?actid=<?php echo $tmpActID ?>&genno=<?php echo $tmpGenNo ?>&list=<?php echo $tmpList ?>",
                "dataSrc": ""
            },
            "columns": [
                { "data": "no" },                            
                { "data": "register" },
                { "data": "name" },
                { "data": "depart" },
                { "data": "tel" },
                { "data": "button" },
                { "data": "edit" }
            ] ,
             columnDefs: [
                {
                    targets: [5,1,0],
                    className: 'dt-body-center'
                }
              ]
        });
        $.fn.DataTable.ext.errMode = 'none';            
        
       
        $('#btnAllSuccess').click(function(){
            swal({
                title: "ต้องการอนุมัติทั้งหมดใช่หรือไม่",
                text: "การอนุมัติทั้งหมดนี้ จะอนุมัติให้สถานะ รออนุมัติเท่านั้น",
                icon: "warning",
                buttons: true,
                dangerMode: false,
              })
              .then((willDelete) => {
                if (willDelete) {                    
                    $.ajax({
                        method: "POST",
                        url: "manage-name-all-success.php",
                        data: {actid: '<?php echo $tmpActID ?>'},
                        success: function(result) { 
                           location.reload();
                        }
                    })                    
                } 
              });
        })
        
        $('#btnList<?php echo $tmpList ?>').addClass('btn-fill');      
        $('#cmdSearch').click(function(){
            var tmpSearch = ($("#txtSearch").val());
            var tmpActID = "<?php echo $tmpActID; ?>";
            var tmpGenNo = "<?php echo $tmpGenNo; ?>";
            window.location = "?actid=" + tmpActID + "&genno=" + tmpGenNo + "&search=" + tmpSearch;
        })

        $('#cmdAll').click(function(){
            var tmpActID = "<?php echo $tmpActID; ?>";
            var tmpGenNo = "<?php echo $tmpGenNo; ?>";
           window.location = "?actid=" + tmpActID + "&genno=" + tmpGenNo;
        })
        
        $('#selGenNo').change(function(){
            var tmpSearch = ($("#txtSearch").val());
            var tmpActID = "<?php echo $tmpActID; ?>";
            var tmpGenNo = this.value;
            window.location = "?actid=" + tmpActID + "&genno=" + tmpGenNo + "&search=" + tmpSearch;
        })
        
    </script>
    
     <style>
        
            .tbMain_length{
               font-size:20px !important;
            }
         
            .dataTables_length, #tbMain_filter, .dataTables_info, #tbMain_paginate{
                font-size:18px;
            }

            .dataTables_length{
                margin-top:8px;
                margin-left:15px;
                 font-size:20px !important;
            }         

            .dataTables_filter{
                width:50%;
            }          

            .dataTables_filter input{       
                width:400px !important;
                font-size:16px;
            } 

            .dataTables_filter input{
                border:1px solid;              
            }
            
       </style>     

</html>
