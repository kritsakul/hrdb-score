<?php

header("Content-type:text-plain; charset=UTF-8");
require_once("connections/conn.php");
require_once("function.inc.php");
require_once __DIR__ . '/vendor/autoload.php';

$roundno = trim($_GET["roundno"]);
$detailno = trim($_GET["detailno"]);
$token = trim($_GET["token"]);
$date=date('Y-m-d');

$sql = "
    SELECT 
        *
    FROM             
        score_round, score_detail
    WHERE
        score_round.round_no=score_detail.round_no AND
        score_round.round_no=:roundno AND
        score_detail.detail_no=:detailno AND
        score_detail.token=:token AND
        score_round.round_unpublish_date>=:date
    LIMIT
        0,1
";    
$stmt = $conn->prepare($sql);
$stmt->bindParam(':roundno',$roundno);
$stmt->bindParam(':detailno',$detailno);
$stmt->bindParam(':token',$token);
$stmt->bindParam(':date',$date);
$stmt->execute();


//ใช้ font sarabun
$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

$mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'orientation' => 'P',
    'format' => 'A4',
    'fontDir' => array_merge($fontDirs, [
        __DIR__ . '/custom/font/directory',
    ]),
    'fontdata' => $fontData + [
        'sarabun' => [
            'R' => 'THSarabunNew.ttf',
            'I' => 'THSarabunNew Italic.ttf',
            'B' => 'THSarabunNew Bold.ttf',
            'BI' => 'THSarabunNew BoldItalic.ttf',
        ]
    ],
    'default_font' => 'sarabun'
]);

ob_start();
?>

<style>
    
    @page {
        margin-top:1.5cm;
        margin-right:1.5cm;
        margin-bottom:1cm;
        margin-left:2.5cm;
    }
    p.tab{margin-left:1.5cm;}
    
    .font14{font-size:14pt;}
    .font16{font-size:16pt;}
    .font18{font-size:18pt;}
    
    .left{text-align:left;}
    .center{text-align:center;}
    .right{text-align:right;}
    
    table tr td{
        border:1px solid;
    }
    th{
        border:1px solid;
    }
    
</style>   

<div class="row font16" id="mainpage">

        <div class="container">
            <div class="font18 center" >
                <u><b>ใบลาป่วย ลาคลอดบุตร ลากิจส่วนตัว</b></u>
            </div>
        </div>

        <div class="container" style="margin-top:8pt;">
            <div style="margin-left:102mm" >
                เขียนที่ สำนักงานปลัดกระทรวงสาธารณสุข<br>
            </div>
            <div style="margin-left:102mm" >
                <?php echo formaldate(date('Y-m-d')) ?>
            </div>
        </div>

        <div class="container" style="margin-top:8pt;">
            <div>
                เรื่อง&nbsp;&nbsp;&nbsp;&nbsp;ข้อมูลผลการสอบ<br>
                เรียน&nbsp;&nbsp;&nbsp;&nbsp;ผู้อำนวยการกองบริหารทรัพยากรบุคคล
            </div>
        </div>    

        <div class="container" style="margin-top:10pt;">
            <div>
                <?php
                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าพเจ้า $name  ตำแหน่ง $posname $levelname  ";                    
                    echo 'สังกัด '.$depname.' กองบริหารทรัพยากรบุคคล สำนักงานปลัดกระทรวงสาธารณสุข ';
                    echo "ขอ$leavemaintypename เนื่องจาก$cause ตั้งแต่<b><u>$fromdate</u></b> ถึง <b><u>$todate</u></b> มีกำหนด <b><u>$dayleave</u></b> วัน $leavetype";
                    if($lasttypename == '-'){ //แปลว่าไม่มีประวัติการลา
                        echo "ข้าพเจ้าได้ลา $lasttypename ครั้งสุดท้ายตั้งแต่วันที่ -   เดือน -  พ.ศ. -  &nbsp;ถึงวันที่ -  เดือน  -   พ.ศ.  -    มีกำหนด  -  วัน ";
                    }else{
                        echo "ข้าพเจ้าได้ $lasttypename ครั้งสุดท้ายตั้งแต่ $lastfromdate ถึง $lasttodate มีกำหนด $lastdays ";
                    }                        
                    echo "ในระหว่างลาจะติดต่อข้าพเจ้าได้ที่ $contact&nbsp; หมายเลขโทรศัพท์ $tel<br>"                    
                ?>
            </div>
        </div>

        <div class="container" style="margin-top:20pt;">            
            <div style="margin-left:67mm" >                    
                <div style="width:350px; display:inline-block;">
                    <div style="text-align:center;">(ลงชื่อ)....................................................</div>
                    <div style="text-align:center;"><?php echo "&nbsp;&nbsp;&nbsp;&nbsp;($name)" ?></div>
                </div> 
            </div>
        </div>

        <div class="font16 container" style="margin-top:20pt;"> 

            <div style="float:left; width:45%;">
                <u>สถิติการลาในปีงบประมาณนี้</u>
                <table cellspacing="0" style="width:8cm;" autosize="1" class="font14">
                    <tr>
                        <th>ประเภทการลา</th>
                        <th>ลามาแล้ว</th>
                        <th>ลาครั้งนี้</th>
                        <th>รวมเป็น</th>
                    </tr>

                    <?php
                        //เช็คข้อมูลประวัติการลาของ ผู้ใช้ทั้งหมด
                        //ต้องนับที่ status=5 เท่านั้น เพราะคือประวัติที่รับอนุญาติแล้วเท่านั้น
                        $sql = "
                            SELECT 
                                emp_leave.*,
                                emp_leave_type.name, 
                                sum(emp_leave.days) as sum_leave
                            FROM
                                emp_leave
                                LEFT JOIN emp_leave_type ON emp_leave.leave_type_code = emp_leave_type.code
                            WHERE 
                                emp_leave.user_id = :userid
                                AND emp_leave.year_budget = :yearbudget
                                AND emp_leave.status = '5' 
                                AND emp_leave.leave_no < :leaveno 
                                AND emp_leave_type.name NOT LIKE '%ลาพักผ่อน%'
                            GROUP BY 
                                leave_type_code
                        ";
                        $stmt = $conn->prepare($sql);
                        $stmt->bindParam(':userid',$tmpUserID);
                        $stmt->bindParam(':yearbudget',$tmpYearBudget);
                        $stmt->bindParam(':leaveno',$leaveno);
                        $stmt->execute();
                        $tmpCheckType = "F";
                        if($stmt->rowCount() > 0){
                            while($resulthistory = $stmt->fetch(PDO::FETCH_ASSOC)){                                                                                    
                                //ถ้าเป็นประเภทการลาเดียวกัน จะแสดงคอลัมน์ ลาครั้งนี้กี่วัน
                                if($resulthistory['leave_type_code'] == $tmpLeaveType){
                                    $thisleave = number_format($result['days'],1);
                                    $total = number_format(intval($resulthistory['sum_leave']) + intval($thisleave),1);
                                    $tmpCheckType = "T";
                                }else{
                                    $thisleave = 0;
                                    $total = number_format(intval($resulthistory['sum_leave']),1);
                                }           
                                echo "<tr>";
                                    echo "<td>" . $resulthistory['name']  . "</td>";
                                    echo "<td>" . $resulthistory['sum_leave'] . "</td>";
                                    echo "<td>" . $thisleave . "</td>";
                                    echo "<td>" . $total . "</td>";
                                echo "</tr>";
                            }
                        }        

                        //เช็คว่า ประเภทการลานี้ เคยมีการลามาแล้วหรือไม่ 
                        //หากยังไม่มี จะต้องสร้าง row ใหม่ แสดงการลาประเภทนี้
                        if($tmpCheckType == "F"){ 
                            echo "<tr>";
                                    echo "<td>" . $leavemaintypename  . "</td>";
                                    echo "<td>" . 0 . "</td>";
                                    echo "<td>" . number_format($result['days'],1) . "</td>";
                                    echo "<td>" . number_format($result['days'],1) . "</td>";
                                echo "</tr>";
                        }                                                                  
                    ?>
                </table>

                <div class="center font14"  style="margin-top:2cm; margin-left:-30px;">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(ลงชื่อ) .................................................. ผู้ตรวจสอบ
                    <br>
                    <?php echo $approve1name ?><br>
                    <?php echo $approve1position ?><br>
                    วันที่  ........./........................../..............
                </div>

            </div>

            <div style="float:left; width:55%; margin-left:30px;">

                <div class="center font14" >
                    <div class="font18 left"><b><u>ความเห็นผู้บังคับบัญชา</u></b></div>
                    <div class="left" style="margin-bottom:10px;">
                        .......................................................................................................<br>
                        .......................................................................................................
                    </div>
                    (ลงชื่อ) .....................................................<br>
                    <?php echo $approve3name ?><br>
                    <?php echo $approve3support . "หัวหน้า" . str_replace(" ","<br>",$result['dep_name']) ?><br>
                    วันที่  ........./........................../..............
                </div>

                <div class="center font14" style="margin-top:10px;" >
                    <div class="font18 left"><b><u>คำสั่ง</u></b></div>
                    &#9634; อนุญาต&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9634; ไม่อนุญาต
                    <div class="left" style="margin-bottom:10px;">
                        .......................................................................................................<br>
                        .......................................................................................................
                    </div>                   
                    (ลงชื่อ) .....................................................<br>
                    <?php echo $approve4name ?><br>
                    <?php echo $approve4support . "ผู้อำนวยการกองบริหารทรัพยากรบุคคล" ?><br>
                    วันที่  ........./........................../..............
                </div>

            </div>

        </div>

</div>


<?php
$html = ob_get_contents();
ob_end_clean();


$mpdf->WriteHTML($html);
$mpdf->Output();
?>