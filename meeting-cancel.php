<?PHP

require_once("connections/conn.php");
require_once("user.inc.php");
require("redirect.inc.php");

//เช็คว่าเป็น Admin หรือไม่
header("Content-type:text-plain; charset=UTF-8");
//วิธีการเช็คว่า ได้ประกาศตัวแปรแบบ define หรือไม่
if (defined('Username')){ //ถ้าประกาศตัวแปรแล้ว
}else{//ถ้ายังไม่ประกาศ
    exit();     
}

//รับค่าข้อมูล
$getData = filter_input(INPUT_POST, 'totalData');
if(trim($getData) == ""){
    exit();    
}

//รับค่าข้อมูล
$getData = filter_input(INPUT_POST, 'totalData');
$detail = explode("|||", $getData);
$tmpActID = trim($detail[0]);
$tmpComment = trim($detail[1]);
$tmpStatus = 0;

$stmt = $conn->prepare("
                UPDATE meet_activity SET
                    act_status = :status2,
                    act_comment = :comment
                    WHERE act_id=:id        
            ");

$stmt->bindParam(':id', $tmpActID);
$stmt->bindParam(':status2', $tmpStatus);
$stmt->bindParam(':comment', $tmpComment);
$stmt->execute();

echo "1";

?>