<?PHP

    header("Content-type:text-plain; charset=UTF-8");
    setcookie('cookUsername', "", time() - 3600); // empty value and old timestamp
    setcookie('cookName', "", time() - 3600); // empty value and old timestamp
    unset($_COOKIE['cookUsername']);
    unset($_COOKIE['cookName']);
    header( "location: login.php" );

?>