<?PHP

header("Content-type:application/json; charset=UTF-8");
require_once("connections/conn.php");

//รับค่าข้อมูล
$username =  trim($_GET["username"]);
$password =  trim($_GET["password"]); //md5 มาเลย
$api =  trim($_GET["api"]); //md5 มาเลย

//login ได้เฉพาะ กลุ่มงาน สรรหา->งานสอบแข่งขัน เท่านั้น
//และ กลุ่มงาน pms&it->admin เท่านั้น
//work_id=32 คือ งาน it
//work_id=10 คือ งาน สอบแข่งขัน
//work_id=38 คือ หัวหน้ากลุ่มงาน สรรหา)
$sql = "
    SELECT 
        * 
    FROM 
        user 
    WHERE 
        username=:username AND 
        md5(password)=:password AND
        md5(api)=:api AND
        status=1 AND
        (
            (permission=1) ||
            (work_id=10) ||
            (work_id=38)
        )
";
$stmt = $conn->prepare($sql);
$stmt->bindParam(':username', $username);
$stmt->bindParam(':password', $password);
$stmt->bindParam(':api', $api);
$stmt->execute();

$dataStmt = $stmt->fetch(PDO::FETCH_ASSOC);
$tmpCount = $stmt->rowCount();
$tmpName = $dataStmt['name'];    

if($tmpCount == 1){ //มีข้อมูล
    setcookie("cookUsername", $username, time() + (7 * 24 * 60 * 60)); // 1 อาทิตย์
    setcookie("cookName", $tmpName, time() + (7 * 24 * 60 * 60)); // 1 อาทิตย์
    header('location: index.php');
    
}else{ //ไม่มีข้อมูล
    header('location: login.php');
    
}        
    
?>